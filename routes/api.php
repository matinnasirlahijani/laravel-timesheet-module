<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\apiAuthenticationController;
use App\Http\Controllers\api\timesheetController;
use App\Http\Controllers\api\clientsController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::middleware('auth:api')->get('/login', [apiAuthenticationController::class , 'login'] );

Route::group(['middleware' => []], function(){
    Route::post('apiLogin' , [apiAuthenticationController::class , 'login']);

    Route::get('getUserByToken/{token}' , [apiAuthenticationController::class , 'getUserByToken']);

    Route::get('latestTimesheet' , [timesheetController::class , 'getLatestTimesheet']);

    Route::post('addTimesheet' , [timesheetController::class , 'addTimesheet']);

    Route::get('getClients' , [clientsController::class , 'getClients']);
});
