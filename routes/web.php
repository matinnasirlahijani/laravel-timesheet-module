<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;
use App\Http\Controllers\filterController;
use Illuminate\Support\Facades\DB;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/mail', function(){
    return view('mail.newrecord');
});


Route::get('/', function () {
    return view('tlogin');
})->name('login');

Route::post('/check',[MainController::class, 'check'])->name('check');

// Route::group(['middleware' => ['AuthCheck']], function(){


// });

Route::group(['middleware' => ['auth']], function(){
    Route::get('/reg', function () {
        return view('reg');
    });
});
Route::group(['middleware' => ['auth']], function(){

    //Route::get('/tlogin',[MainController::class, 'login']);
    //Route::get('/reg',[MainController::class, 'register']);


    Route::get('/destroyTimesheet/{id}' , [MainController::class , 'destroyTimesheet'])->name('destroyTimesheet');

    Route::get('/pdf',[MainController::class,'generate_pdf'])->name('pdf');

    Route::get('/tlogin', function () {
       return view('tlogin');
    });

    Route::get('/addclient',['middleware' => 'admin', function(){
        return view('addClient');
    }])->name('addclient');

    Route::get('/save',[MainController::class, 'save'])->name('save');

    Route::post('/newClientAdded',[MainController::class,'AddClient'])->name('addClient');

    //Route::get('file-upload', [MainController::class ,'AddClient'])->name('fupload');

    Route::get('/tdashboard',[MainController::class, 'dashboardView'])->name('tdashboard');

    Route::get('/add', [MainController::class , 'addView'])->name('add');

    Route::get('/editMyTimeSheet/{id}' , [MainController::class , 'editMyTimeSheet'])->name('editMyTimeSheet');

    Route::post('/addleave' , [MainController::class , 'addleave'])->name('addleave');

    Route::get('/leave' , [MainController::class , 'leave'])->name('leave');

    Route::get('/editMyLeave/{id}' , [MainController::class , 'editMyLeave'])->name('editMyLeave');

    Route::post('/updateMyLeave' , [MainController::class , 'updateMyLeave'])->name('updateMyLeave');

    Route::post('/added',[MainController::class, 'add'])->name('added');

    Route::get('/updateTimesheet' , [MainController::class , 'updateTimesheet'])->name('updateTimesheet');

    Route::get('/generateClientpdf' , [filterController::class , 'clientpdf'])->name('generateClientpdf');

    Route::get('/generatefilterpdf' , [filterController::class , 'filterpdf'])->name('generatefilterpdf');



    Route::post('downloadMyPDF' , [MainController::class , 'downloadMyPDF'])->name('downloadMyPDF');

    // Route::get('userinfo', [MainController::class, 'userinfo']);

    Route::post('/filter',[filterController::class, 'employerFilter'])->name('filter');

    Route::post('/filterByClient',[filterController::class, 'filterByClient'])->name('filterByClient');

    Route::get('/logout',[MainController::class, 'logout'])->name('logout');

    Route::get('deleteMyTimesheet/{id}',[MainController::class , 'deleteMyTimesheet'])->name('deleteMyTimesheet');

    Route::get('deleteMyLeave/{id}' , [MainController::class, 'deleteMyLeave'])->name('deleteMyLeave');


});

Route::get('timediff/{time_in}/{time_out}/{is_friday}' , [MainController::class , 'timediff']);


