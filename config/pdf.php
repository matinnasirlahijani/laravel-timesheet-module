<?php

return [
	'mode'                  => 'utf-8',
	'format'                => 'A4',
	'author'                => 'Matt',
	'subject'               => 'Test',
	'keywords'              => '',
	'creator'               => 'Laravel Pdf',
	'display_mode'          => 'fullpage',
	'tempDir'               => base_path('../temp/'),
	'pdf_a'                 => false,
	'pdf_a_auto'            => false,
	'icc_profile_path'      => '',
	'font_path' => base_path('resources/fonts'),
	'font_data' => [
		'examplefont' => [
			'R'  => 'IRANSansWeb(FaNum).ttf',    // regular font
			'B'  => 'IRANSansWeb(FaNum).ttf',       // optional: bold font
			'I'  => 'IRANSansWeb(FaNum).ttf',     // optional: italic font
			'BI' => 'IRANSansWeb(FaNum).ttf', // optional: bold-italic font
			'useOTL' => 0xFF,    // required for complicated langs like Persian, Arabic and Chinese
			'useKashida' => 75,  // required for complicated langs like Persian, Arabic and Chinese
		]
		// ...add as many as you want.
	]
];
