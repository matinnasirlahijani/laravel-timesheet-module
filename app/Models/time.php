<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class time extends Model
{
    use HasFactory;

     protected $table = "admins_time";
     protected $primarykey = "id";
     protected $fillable = ['client' , 'service' , 'hours','date','client_time_id'];
     public function timesheet(){
         return $this->belongTo(Admin::class);
    }
}
