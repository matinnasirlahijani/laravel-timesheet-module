<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class clientsheet extends Model
{
    use HasFactory;
    protected $table = "clientsheets";
    protected $primarykey = "id";
    protected $fillable = ['clientName','upload_file'];
    
    public function timesh()
    {
        return $this->hasMany(time::class);
    }

    public function findPDF()
    {
        return $this->hasMany(clientpdf::class);
    }

   
}
