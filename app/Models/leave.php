<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class leave extends Model
{
    use HasFactory;
    protected $table = "leaves";
    protected $primarykey = "id";
    protected $fillable = ['id','user_id','time_in','time_out'];
}
