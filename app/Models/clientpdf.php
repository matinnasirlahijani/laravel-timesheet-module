<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class clientpdf extends Model
{
    use HasFactory;
    protected $table = 'clientpdfs';
    protected $primarykey = 'id';
    protected $fillable = ['clientID','pdf'];

    public function clipdf()
    {
        return $this->belongsTo(clientsheet::class);
    }
}
