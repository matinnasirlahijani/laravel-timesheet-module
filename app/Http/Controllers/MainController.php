<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use App\jalali;
use App\Libraries\jdf;
use Hekmatinasser\Verta\Verta;
use App\Models\time;
use App\Models\admins_2;
use App\Models\clientsheet;
use App\Models\leave;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use App\Events\newrecord;
use Illuminate\Support\Facades\Auth;
use App\Jobs\MailJob;
use Illuminate\Support\Str;
use App\Models\clientpdf;
use App\Http\Controllers\filterController;
use PDF;
use Carbon\Carbon;
class MainController extends Controller
{
    // validation while sign up a new user and save it to database
    public $year;
    function __construct(){
        $verta = new Verta;
        $this->year = $verta->year;
        // $this->year = 1401;
    }



    function save (Request $request)
    {
        //return $request->input();
        $request->validate([
            'name'=> 'required',
            'email'=>'required|email|unique:admins',
            'password'=> 'required',
            'password_confirm'=> 'required'

        ]);
        $admin = new Admin;

        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->password = Hash::make($request->password);
        $admin->api_token = Str::random(60);

        $save = $admin->save();

        if($save){
            $res = ['loggedin' =>'yes'];
            return view('reg' , $res);
        } if(! $save){
            $res = ['loggedin' =>'no'];
            return view('reg' , $res['loggedin']);
        }
    }
    // validate sign in email and password and redirect the user to dashboard
         function check (Request $request){

            $request->validate([
                    'email' => 'required|email',
                    'password' => 'required'
                ]);

            $userInfo = Admin::where('email','=', $request->email)->first();
            $credentials = $request->only('email', 'password');

            // dd($userInfo);

            if (Auth::attempt($credentials)) {
                $request->session()->put('is_admin', $userInfo->is_admin);
                $request->session()->put('LoggedUser' , $userInfo->id);
                $request->session()->put('UserName', $userInfo->name);
                // dd(session::get('is_admin'));
                return redirect()->route('tdashboard');
            }
                else {
                            return back()->with('fail','پسورد یا نام کاربری اشتباه است.');
                        }

                // if(!$userInfo)
                //     {
                //         return back()->with('fail','Email not detected');
                //     }

                // else
                //      {
                // if(Hash::check($request->password, $userInfo->password))
                //     {
                //         $request->session()->put('is_admin', $userInfo->is_admin);
                //         $request->session()->put('LoggedUser' , $userInfo->id);
                //         $request->session()->put('UserName', $userInfo->name);
                //         Auth::attempt(['email' => $userInfo->email, 'password' => $userInfo->password]);
                //         return redirect('tdashboard');
                //     }
                // if( Auth::attempt(['email' => $userInfo->email, 'password' => Hash::check($request->password, $userInfo->password)])){
                //     dd('yes');
                // }
                    // else {
                    //         return back()->with('fail','Incorrect Password');
                    //     }
        // }
    }

            function dashboardView()
            {
                $admins = Admin::query()->where('is_active' , '=' , true)->get();
                $times = time::all();
                $clients = clientsheet::all();
                $data = time::orderBy('created_at', 'DESC')->get();
                $a = DB::table('admins_time')->orderBy('geregorian' , 'desc')->paginate(10);

                $data = ['LoggedUserInfo'=>Admin::where('id' ,'=' ,session('LoggedUser'))->first()];
                $get_admin = session::get('is_admin');
                if($get_admin)
                {
                    return view('tdashboard', compact('data','a','admins','clients'));
                }
                else
                {
                    return redirect('add')->with($data);
                }

                // return view('tdashboard',compact('data','a','admins','clients'));

                }


        function logout()
        {
            // if(session()->has('LoggedUser'))
            // {
            //     session()->pull('LoggedUser');
            //     return redirect('tlogin');
            // }
            Session::flush();

        Auth::logout();
        return redirect('tlogin');
        }


            function addView()
            {
                $userid = session('LoggedUser');
                $date = Carbon::now()->subDays(60);
                $history = DB::table('admins_time')
                ->where('admin_time_id' ,'=' ,$userid)
                ->where('created_at' , '>=' , $date )
                ->orderBy('geregorian' , 'desc')->get();
                return view('add' , compact('history'));
            }

            function editMyTimeSheet($id)
            {
                $clients = DB::table('clientsheets')->get();
                $findTimesheetId = DB::table('admins_time')
                ->where('id' ,'=' , $id)->get();
                return view('editMyTimesheet' , compact("findTimesheetId","clients","id"));
            }

            function updateTimesheet(Request $request)
            {
                $id = $request->id;
                $client = $request->client;
                $fiscalYear = $request->fiscalYear;
                $service = $request->service;
                $task = $request->task;
                $duty = $request->duty;
                $month = $request->month;
                $time_in = $request->time_in;
                $time_out = $request->time_out;
                $lunch = $request->lunch;
                $date = $request->date;

                //find Client ID
                $getClient_id = DB::table('clientsheets')->where('clientName' , '=' , $client)->get();

                foreach ($getClient_id as $myClientId)
                {
                   $client_id = $myClientId->id;
                  // dd($client_id);
                }


                //calculate Time Diff
                $isFriday = $request->date;
                $time_in = $request->time_in;
                $time_out = $request->time_out;

                $time_dif = $this->timediff($time_in, $time_out , $isFriday);

                if($lunch == 1){
                    $carbon = new Carbon($time_dif);
                    $time_dif = ($carbon->subMinutes(30)->format('H:i:s'));
                    // dd($time_dif);
                    // die();
                   }

                $dateExplode = (explode(" ",$request->date));
                $sortedArray = $this->TrimShamsiDateToMiladiDate($dateExplode);
                $getEnLocale = $this->FaToEnLocale($sortedArray[2],$sortedArray[1],$sortedArray[0]);
                $geregorian = implode("/",Verta::getGregorian($getEnLocale[0],$getEnLocale[1],$getEnLocale[2]));


                time::where('id' , $id)
                ->update(['client' => $client ,
                'service' => $service,
                'fiscalYear' => $fiscalYear,
                'task' => $task,
                'month' => $month,
                'duty' => $duty,
                'time_in' => $time_in,
                'time_out' => $time_out ,
                'lunch' => $lunch,
                'date' => $date,
                'geregorian' => $geregorian,
                'hours' => $time_dif,
                'client_time_id' => $client_id,
                'comments' => $request->comment
                ]);

                return redirect()->back()->with('ChangesDone' , 'اطلاعات با موفقیت ویرایش شد');
            }


        function add (Request $request)
        {

            $request->validate([
                'client'=> 'required',
                'service'=>'required',
                //'hours'=> 'required',
                'date' => 'required',
                'fiscalYear' => 'required',
                'month' => 'required',
                'task' => 'required',
                'time_in' => 'required',
                'time_out' => 'required',
            ]);
            $dateExplode = (explode(" ",$request->date));
            if($request->lunch)
            {
                //dd('yes');
                $lunch = 1;
            }else if(!$request->lunch){
               // dd('no');
                $lunch = 0;
            }

            if($request->duty)
            {
                //dd('yes');
                $duty = 1;
            }else if(!$request->duty){
               // dd('no');
                $duty = 0;
            }
            $comment = $request->comment;
            if(!$request->comment)
            {
                $comment = 'ندارد';
            }
            //Add Year
            $verta = new Verta;
            $year = $this->year;
            $clientname = $request->client;
            //Get the User ID
            $userInfo = Admin::where('id','=', session('LoggedUser'))->first();
            session()->put('UserName' , $userInfo->name);
            $user = session();
           //Calculate Diff Between Time-in and Time-out
           $time_in = $request->time_in;
           $time_out = $request->time_out;
           $isFriday = $request->date;
           $time_dif = $this->timediff($time_in, $time_out ,$isFriday);
           if($lunch){
            $carbon = new Carbon($time_dif);
            $time_dif = ($carbon->subMinutes(30)->format('H:i:s'));
            // dd($time_dif);
            // die();
           }


           //Shmasi To Miladi Conversion
           $getMiladiDate = $request->date;

        //    if(strlen($dateExplode) >)
        //    dd($dateExplode);


           $sortedArray = $this->TrimShamsiDateToMiladiDate($dateExplode);
           $getEnLocale = $this->FaToEnLocale($sortedArray[2],$sortedArray[1],$sortedArray[0]);
           $geregorian = implode("/",Verta::getGregorian($getEnLocale[0],$getEnLocale[1],$getEnLocale[2]));




           $clientsheet = DB::table('clientsheets')->get();

            $timesheet = new time;
            $timesheet->client = $request->client;
            $timesheet->fiscalYear = $request->fiscalYear;
            $timesheet->time_in = $request->time_in;
            $timesheet->time_out = $request->time_out;
            $timesheet->service = $request->service;
            $timesheet->task = $request->task;
            $timesheet->month = $request->month;
            $timesheet->duty = $duty;
            $timesheet->lunch = $lunch;
            $timesheet->year = $year;
            $timesheet->hours = $time_dif;
            $timesheet->date = $request->date;
            $timesheet->geregorian = $geregorian;
            $timesheet->admin_time_id = session()->get('LoggedUser');
            $timesheet->name = $userInfo->name;
            $timesheet->comments = $comment;
            $getclientid = DB::table('clientsheets')->where('clientName' ,'=' , $clientname)->get();

            foreach ($getclientid as $getid)
            {
                //echo(". $getid->id");
            }

            $timesheet->client_time_id = $getid->id;
           // dd($getclientid);

            $timesheet->save();

           // event(new newrecord(Request()->all(),Session::all()));
          // $reqall = Request()->all();
         //$this->dispatch(new MailJob($reqall));
        // echo($time_dif)->format('%H:%I ');

             return redirect()->back()->with('added' , 'رکورد با موفقیت اضافه شد');



        }

        function downloadMyPDF(Request $request){
            // dd(session::get('id')); die();
            $userid = session('LoggedUser');
            $username = session::get('UserName');
            $month = $request->month;
           $this->generateMypdf($username , $month);
        }

        function destroyTimesheet($id)
        {
            time::destroy($id);
            dd('Done');
        }

        function deleteMyTimesheet($id)
        {

            Time::find($id)->delete();
            return back()->with('Delete-Done','رکورد با موفقیت حذف شد');

        }

        function deleteMyLeave($id)
        {
            leave::find($id)->delete();
            return back()->with('Delete-leave-Done','رکورد با موفقیت حذف شد');
        }



        function TrimShamsiDateToMiladiDate($date)
        {
            $deleteValues = array('شنبه' , 'یک' ,'یکشنبه' , 'دو' ,'دوشنبه','سه', 'سه شنبه', 'چهار','چهارشنبه','پنج' ,'پنج شنبه' , 'پنج‌شنبه' , 'جمعه');

                $findArrayKey = array_intersect($date,$deleteValues);

                foreach($findArrayKey as $fundedArray)
                {
                    $key = array_search($fundedArray, $date);
                    unset($date[$key]);

                }
                return array_values($date);
        }

        function FaToEnLocale($year,$month,$day)
        {
            $persian = array('۰', '۱', '۲', '۳',
             '۴', '۵', '۶',
              '۷', '۸', '۹'
            //   ,'۱۰' ,'۱۱','۱۲'
            //   , '۱۳','۱۴' ,'۱۵',
            // '۱۶' , '۱۷' , '۱۸',
            //  '۱۹' , '۲۰' , '۲۱',
            //   '۲۲' , '۲۳' , '۲۴',
            //   '۲۵' , '۲۶' , '۲۷',
            //    '۲۸' , '۲۹' , '۳۰',
            //   '۳۱' , '۳۲'
            );
            $convertedMonths = array('1' , '2' , '3' , '4' , '5' , '6' , '7' , '8' , '9' , '10' , '11' , '12');
            $persianMonths = array('فروردین' , 'اردیبهشت' , 'خرداد'
            , 'تیر' , 'مرداد' , 'شهریور'
             , 'مهر' , 'آبان' , 'آذر'
             , 'دی' , 'بهمن' , 'اسفند');
            $enNumbers = range(0, 9);
            $YearReplace = str_replace($persian,$enNumbers,$year);
            $monthReplace = str_replace($persianMonths,$convertedMonths,$month);
            $dayReplace = str_replace($persian,$enNumbers,$day);
            $toArray = Array($YearReplace,$monthReplace,$dayReplace);
            return $toArray;
        }


        function AddClient (Request $request)
        {
            $request->validate([
                'ClientName' => 'required',
            ]);
            //$getDate = new Verta();
            //$fileName = $getDate . '-' . $request->ClientName .'-' . session::get('UserName').'-'. $request->file->getClientOriginalName();
           // $request->file->move(public_path('Client-pdf'),$fileName);
            //dd($fileName);

            $addClientName = new clientsheet;
           // $addPDF = new clientpdf;

            $addClientName->clientName = $request->ClientName;
           // $addPDF->pdf = $request->file->getClientOriginalName();
            //$addPDF->ClientID = $NextClientID;
            //$addClientName->upload_file = $request->file->extension();

            //$addPDF->save();
            $addClientName->save();

            return view('addClient', $addClientName);
        }

        function leave()
        {
          $userid = session('LoggedUser');

        //   $leaves = DB::table('admins')->where('id' , '=' ,$userid)->get();

        //   foreach ($leaves as $leave)
        //   {

        //     $dayLeaves = $leave->leaves;

        //   }
        $verta = new Verta;
        $year = $this->year;
         //Calculates Remained Leaves
         $totalLeavesInYear = DB::table('leaves')
                ->select('name',DB::raw('SEC_TO_TIME( SUM( TIME_TO_SEC(  CAST(`hours` AS SIGNED)  ) ) ) AS sum'))
                ->groupBy('name')
                ->where('year','=',$year)
                ->where('user_id' , '=' , $userid)
                ->first();

                $filterController = new filterController;
                $getRemainLeaves = 208;
                if(isset($totalLeavesInYear)){

                    $getRemainLeaves = $filterController->ConvertTotalLeavesInYearToHours($totalLeavesInYear->sum);
                }

                // dd($totalLeavesInYear);

        //  dd($getRemainLeaves);

          $leavesQuery = DB::table('leaves')->where('user_id' , '=' , $userid)->get();

            return view('leave',compact('getRemainLeaves','leavesQuery'));
        }

        function editMyLeave($id)
        {
            $FindLeaveById = DB::table('leaves')->where('id' , '=' , $id)->get();

            return view('editMyLeave' , compact('FindLeaveById','id'));

        }

        function updateMyLeave(Request $request)
        {
            $leaveId = $request->id;
            $time_in = $request->time_in;
            $time_out = $request->time_out;
            $month = $request->month;
            $date = $request->date;
            //Calculate Time diff
            $timeDiff = $this->timediff($time_in , $time_out , 'شنبه');

            Leave::where('id' , $leaveId)->update([
                'time_in' => $time_in,
                'time_out' => $time_out,
                'hours' => $timeDiff,
                'month' => $month,
                'date' => $date
            ]);

            return redirect()->back()->with('leaveUpdated' , 'رکورد با موفقیت ویرایش شد');
        }

        function addleave(Request $request)
        {
            $verta = new Verta;
            $year = $this->year;
            $userid = session('LoggedUser');
            $username = session('UserName');
            $request->validate([
                'month' => 'required',
                'date' => 'required',
                'time_in' => 'required',
                'time_out' => 'required'
            ]);
            //get user leave days
            $leaves = DB::table('admins')->where('id' , '=' ,$userid)->get();
            //Calculate Time Diff
           // $carbon = new Carbon;
            $time_in = $request->time_in;
            $time_out = $request->time_out;

            //$time_dif = $time_in->diff($time_out);
            //$time_dif = $time_in->diff($time_out)->format('%H:%I ');
            $time_dif = $this->timediff($time_in,$time_out);
            //dd($time_dif);
          foreach ($leaves as $leave)
          {

            $dayLeaves = $leave->leaves;

          }
          if(!$dayLeaves == 0){
            $dayLeaves -=1;
          }

          //Update user leave days
            Admin::where('id' , $userid)->update(['leaves' => $dayLeaves]);

            //dd($username);
             $leave = new leave;

             $leave->user_id = $userid;
             $leave->name = $username;
             $leave->time_in = $request->time_in;
             $leave->time_out = $request->time_out;
             $leave->hours = $time_dif;
             $leave->date = $request->date;
             $leave->month = $request->month;
            $leave->year = $year;
             $leave->save();

              return redirect()->back()->with('leave-success' , 'درخواست مرخصی با موفقیت ثبت شد');
        }

        function userinfo()
        {
            //dd(session::get('UserName'));

            $times = clientsheet::findOrFail(1)->findPDF;

            foreach($times as $time)
            {
                echo($time->pdf);
            }




        }



        function generate_pdf() {

            $lastMonth = DB::table('admins_time')
            ->where('created_at', '>', now()
            ->subDays(30)->endOfDay())
            ->orderBy('Created_at' , 'desc')
            ->get();
            $pdf = PDF::loadView('pdf', compact('lastMonth'));
            return $pdf->stream('last 30 days report.pdf');
        }


         function timediff($time_in, $time_out,$isFriday = "شنبه")
         {
            $carbon = new Carbon;
            $time_in = new Carbon($time_in);
            $time_dif = $time_in->diff($time_out);

            if(str_contains($isFriday, 'جمعه'))
            {

               //$calculateBonus =  intval($time_dif->format('%H') * 1.4 ) ;
               //$calculateBonusMinutes =  intval($time_dif->format('%i') * 1.4 ) ;
               $minutes = intval($time_dif->format('%I') * 1.4);
               $toint = intval($time_dif->format('%H') * 1.4);
               if($minutes >= 60)
               {
                   $toint = $toint + 1;
                   $minutes = $minutes - 60;
               }
               $merge = "$toint:$minutes" ;
               $calculateBonus =  intval($time_dif->format('%H') * 1.4 ) ;
               //$time_dif = date('h:i',$calculateBonus);
               $time_dif = date("$calculateBonus:$minutes");
               //echo(date("$calculateBonus:I:s"));

               return ($merge);
            }
            else
            {
             $time_dif = $time_in->diff($time_out)->format('%H:%I ');
             return ($time_dif);
            }


         }

         function generateMypdf($name , $month)
       {

        $filterController = new filterController;

        $overtimeHours = $filterController->overTimeHours;
        $overtimeMonths =  array(

                "فروردین",
                'اردیبهشت',
                'خرداد',
                'تیر' ,
                'مرداد' ,
                'شهریور',
                'مهر' ,
                'آبان' ,
                'آذر' ,
                'دی' ,
                'بهمن' ,
                'اسفند'
            );
            // $filterController = new filterController;
            $replaceMonths = (str_replace($overtimeMonths,$overtimeHours,$month));
            $dynamic_overtime = intval($replaceMonths);
            $ConvertTotalLeavesInYearToDays = 0;
            $ConvertTotalLeavesInYearToHours = 0;
            $total = 0;
            $totalLeaves = 0;
            $filter = $name;
            $month = $month;
            $overTime = 0;
            $LeavesSum = 0;
            $RAWtotalHours = 0;
            $verta = new Verta;
            $year = $this->year;
            $calculateTotalIncludLeave = 0;

            $dutyCountByMonth = DB::table('admins_time')
             ->where('name' ,'=',$name)
             ->where('month','=',$month)
             ->where('year','=',$year)
             ->where('duty' , '=', 1)
             ->count();

             $totalDuty = DB::table('admins_time')
             ->where('name' ,'=',$name)
             ->where('year','=',$year)
             ->where('duty' , '=', 1)
             ->count();

             $leavesQuery = DB::table('leaves')
            ->where('name' , '=' ,$name)
            ->where('year','=',$year)
            ->where('month' , '=' , $month)->get();


            $filter = $name;
            $month = $month;
            $userId = DB::table('admins')->where('name' , '=' ,  $filter)->first();

            $getTime =  DB::table('admins_time')
            ->orderBy('geregorian' , 'asc')
            ->where('admin_time_id' , '=' , $userId->id)
            ->where('year','=',$year)
            ->where('month' , '=' , $month)
            ->paginate(500);

            $getclientsum = DB::table('admins_time')
            ->select( 'admin_time_id', DB::raw('SEC_TO_TIME(SUM(TIME_TO_SEC(hours))) as sum'))
            ->groupBy('admin_time_id')
            ->where('admin_time_id' , '=' , $userId->id)
            ->where('year','=',$year)
            ->where('month' , '=' , $month)
            ->get();

            $totalLeaves = DB::table('leaves')
                ->select('name','month',DB::raw('SEC_TO_TIME( SUM( TIME_TO_SEC(  CAST(`hours` AS SIGNED)  ) ) ) AS sum'))
                ->groupBy('name','month')
                ->where('year','=',$year)
                ->where('name' , '=' , $filter)
                ->where('month' , '=' , $month)
                ->first();

           $totalLeavesInYear = DB::table('leaves')
                ->select('name',DB::raw('SEC_TO_TIME( SUM( TIME_TO_SEC(  CAST(`hours` AS SIGNED)  ) ) ) AS sum'))
                ->groupBy('name')
                ->where('year','=',$year)
                ->where('name' , '=' , $filter)
                ->first();


    if($totalLeaves && $getclientsum ){
        $ConvertTotalLeavesInYearToDays = $this->ConvertTotalLeavesInYearToDays($totalLeavesInYear->sum);
        $ConvertTotalLeavesInYearToHours = $this->ConvertTotalLeavesInYearToHours($totalLeavesInYear->sum);

          if(intval($totalLeavesInYear->sum) >= 208){

                foreach($getclientsum as $clientsum){
                    $total = $clientsum->sum;

                }


                $overTime =  $filterController->CalculateOverTime($dynamic_overtime,intval($total));
                // $overTime =  $dynamic_overtime - intval($total) ;

               }
               else if(intval($totalLeavesInYear->sum) <= 208){

                foreach($getclientsum as $getsum){
                    $RAWtotalHours = $getsum->sum;
                }

                    //foreach($totalLeaves as $tLeaves){
                    $LeavesSum = $totalLeaves->sum ;
                    $LeavesSumInt = intval($LeavesSum);
                    $RAWtotalHoursInt = intval($RAWtotalHours);
                //}




                $sumHours = $this->sumHours($LeavesSum,$RAWtotalHours);



                  $total = $sumHours;

                 $intTotal = intval($total);

                $overTime =  $filterController->CalculateOverTime($dynamic_overtime,intval($total));
                //   dd($sumHours);die();



               }
            }
            //if totalLeaves is null
            else{

            $totalLeaves = 0;
            foreach($getclientsum as $getsum){
                $RAWtotalHours = $getsum->sum;
            }

                //foreach($totalLeaves as $tLeaves){
                $LeavesSum = 0 ;
                $LeavesSumInt = 0;
                $RAWtotalHoursInt = intval($RAWtotalHours);
            //}
            $total = $RAWtotalHours;

             $intTotal = intval($total);


             $overTime =  $filterController->CalculateOverTime($dynamic_overtime,$RAWtotalHoursInt);
            //   dd($sumHours);die();


            }



             //leaves
             $leavesQuery = DB::table('leaves')
             ->where('name' , '=' ,$filter)
             ->where('year','=',$year)
             ->where('month' , '=' , $month)->get();

            //  dd($overTime);die();

               $pdf = PDF::loadView('filterpdf' , compact("dynamic_overtime","totalLeavesInYear","ConvertTotalLeavesInYearToHours","ConvertTotalLeavesInYearToDays","total","getTime","dutyCountByMonth","totalDuty" , "getclientsum" , "filter","month","overTime","leavesQuery","totalLeaves","calculateTotalIncludLeave"));
               return $pdf->stream(' تایم شیت');
       }


       function ConvertTotalLeavesInYearToDays($hours) : Int
       {
        $totalLeavesInDay = intval($hours) / 8;
        return 26 - $totalLeavesInDay;

       }

       function ConvertTotalLeavesInYearToHours($hours)
       {
        if($hours >= 208)
        {
            return intval($hours) - 208;
        }else
        {

            return 208 - intval($hours);

        }
       }

       function sumHours($a , $b ){

        if(strlen($a) == 8 && strlen($b) >= 9){
        $a_minutes = substr($a , 3);
        $a_hours = substr($a,0,2);
        $b_minutes = substr($b , 4);
        $b_hours = substr($b , 0,3);
        $SumMinutes = intval($a_minutes) + intval($b_minutes);
        $sumHours = intval($b_hours) + intval($a_hours);
        if($SumMinutes >= 60){
            $sumHours ++ ;
            $SumMinutes = $SumMinutes - 60;
        }
        $glueHoursAndMinutes = $sumHours.':'.$SumMinutes;
        return $glueHoursAndMinutes;
        }

        if(strlen($a) >= 8 && strlen($b) <= 9){
            $a_minutes = substr($a , 3);
            $a_hours = substr($a,0,2);
            $b_minutes = substr($b , 3);
            $b_hours = substr($b , 0,3);
            $SumMinutes = intval($a_minutes) + intval($b_minutes);
            $sumHours = intval($b_hours) + intval($a_hours);
            if($SumMinutes >= 60){
                $sumHours ++ ;
                $SumMinutes = $SumMinutes - 60;
            }
            $glueHoursAndMinutes = $sumHours.':'.$SumMinutes;
            return $glueHoursAndMinutes;
            }
        }




    }

