<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
//use App\jalali;
//use App\Libraries\jdf;
use Hekmatinasser\Verta\Verta;
use App\Models\time;
use App\Models\admins_2;
use App\Models\leave;
use App\Models\clientsheet;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Http\Controllers\MainController;
use PDF;


class filterController extends Controller
{

    //1403
    public $overTimeHours = array(140,192,172,168,200,152,192,192,176,184,176,180);
    public $year;
    function __construct(){
        $verta = new Verta;
        $this->year = $verta->year;
        // $this->year = 1401;
    }

    function employerFilter(Request $request){
        // 1403
         $overtimeHours = $this->overTimeHours;

        $overtimeMonths =  array(

                "فروردین",
                'اردیبهشت',
                'خرداد',
                'تیر' ,
                'مرداد' ,
                'شهریور',
                'مهر' ,
                'آبان' ,
                'آذر' ,
                'دی' ,
                'بهمن' ,
                'اسفند'
            );

            $replaceMonths = (str_replace($overtimeMonths,$overtimeHours,$request->input('month')));
            $dynamic_overtime = intval($replaceMonths);

            $ConvertTotalLeavesInYearToDays = 0;
            $ConvertTotalLeavesInYearToHours = 0;
            $total = 0;
            $totalLeaves = 0;
            $filter = $request->input('nameFilter');
            $month = $request->input('month');
            $overTime = 0;
            $LeavesSum = 0;
            $RAWtotalHours = 0;
            $verta = new Verta;
            $year = $this->year;
            $calculateTotalIncludLeave = 0;


            $dutyCountByMonth = DB::table('admins_time')
             ->where('name' ,'=',$request->input('nameFilter'))
             ->where('month','=',$request->input('month'))
             ->where('year','=',$year)
             ->where('duty' , '=', 1)
             ->count();

             $totalDuty = DB::table('admins_time')
             ->where('name' ,'=',$request->input('nameFilter'))
             ->where('year','=',$year)
             ->where('duty' , '=', 1)
             ->count();

             $leavesQuery = DB::table('leaves')
            ->where('name' , '=' ,$request->input('nameFilter'))
            ->where('year','=',$year)
            ->where('month' , '=' , $month)->get();


            $filter = $request->input('nameFilter');
            $month = $request->input('month');
            $userId = DB::table('admins')->where('name' , '=' ,  $filter)->first();

            $getTime =  DB::table('admins_time')
            ->orderBy('geregorian' , 'asc')
            ->where('admin_time_id' , '=' , $userId->id)
            ->where('year','=',$year)
            ->where('month' , '=' , $month)
            ->paginate(500);

            $getclientsum = DB::table('admins_time')
            ->select( 'admin_time_id', DB::raw('SEC_TO_TIME(SUM(TIME_TO_SEC(hours))) as sum'))
            ->groupBy('admin_time_id')
            ->where('admin_time_id' , '=' , $userId->id)
            ->where('year','=',$year)
            ->where('month' , '=' , $month)
            ->get();

            // dd($getclientsum);

            $totalLeaves = DB::table('leaves')
                ->select('name','month',DB::raw('SEC_TO_TIME( SUM( TIME_TO_SEC(  CAST(`hours` AS SIGNED)  ) ) ) AS sum'))
                ->groupBy('name','month')
                ->where('year','=',$year)
                ->where('name' , '=' , $filter)
                ->where('month' , '=' , $month)
                ->first();

           $totalLeavesInYear = DB::table('leaves')
                ->select('name',DB::raw('SEC_TO_TIME( SUM( TIME_TO_SEC(  CAST(`hours` AS SIGNED)  ) ) ) AS sum'))
                ->groupBy('name')
                ->where('year','=',$year)
                ->where('name' , '=' , $filter)
                ->first();


    if($totalLeaves && $getclientsum ){
        $ConvertTotalLeavesInYearToDays = $this->ConvertTotalLeavesInYearToDays($totalLeavesInYear->sum);
        $ConvertTotalLeavesInYearToHours = $this->ConvertTotalLeavesInYearToHours($totalLeavesInYear->sum);

        // dd($totalLeavesInYear->sum,$ConvertTotalLeavesInYearToHours );

          if(intval($totalLeavesInYear->sum) >= 208){

                foreach($getclientsum as $clientsum){
                    $total = $clientsum->sum;
                    // dd($total);

                }


                // dd(gettype($total));
                // $intTotal = intval($total);


                $overTime = $this->CalculateOverTime($dynamic_overtime,intval($total));



               }
               else if(intval($totalLeavesInYear->sum) <= 208){

                foreach($getclientsum as $getsum){
                    $RAWtotalHours = $getsum->sum;
                }

                    //foreach($totalLeaves as $tLeaves){
                    $LeavesSum = $totalLeaves->sum ;
                    $LeavesSumInt = intval($LeavesSum);
                    $RAWtotalHoursInt = intval($RAWtotalHours);
                //}




                $sumHours = $this->sumHours($LeavesSum,$RAWtotalHours);



                  $total = $sumHours;

                 $intTotal = intval($total);

                $overTime = $this->CalculateOverTime($dynamic_overtime,intval($total));
                //   dd($sumHours);die();



               }
            }
            //if totalLeaves is null
            else{

            $totalLeaves = 0;
            foreach($getclientsum as $getsum){
                $RAWtotalHours = $getsum->sum;
                // dd($RAWtotalHours);
            }

                //foreach($totalLeaves as $tLeaves){
                $LeavesSum = 0 ;
                $LeavesSumInt = 0;
                $RAWtotalHoursInt = intval($RAWtotalHours);
            //}
            $total = $RAWtotalHours;

             $intTotal = intval($total);

            //  dd($intTotal);

            //  if($dynamic_overtime > $RAWtotalHoursInt){
            //     $overTime = $dynamic_overtime - $RAWtotalHoursInt ;
            //  }else if($RAWtotalHoursInt > $dynamic_overtime){
            //     $overTime = $RAWtotalHoursInt - $dynamic_overtime ;
            //  }

            $overTime = $this->CalculateOverTime($dynamic_overtime,$RAWtotalHoursInt);

            //   dd($sumHours);die();


            }
            // dd($total);die();
            // dd($total);die();

            return view('filter', compact("dynamic_overtime","totalLeavesInYear","ConvertTotalLeavesInYearToHours","ConvertTotalLeavesInYearToDays","total","getTime","dutyCountByMonth","totalDuty" , "getclientsum" , "filter","month","overTime","leavesQuery","totalLeaves","calculateTotalIncludLeave"));
    }


        function CalculateOverTime($dynamic_overtime,$total) : Int{
//        dd($dynamic_overtime , $total);
            if($dynamic_overtime >= intval($total)){
                $overTime =  0;
//                dd($overTime);
                // $overTime = 0;
            }else{
                $overTime =  intval($total) - $dynamic_overtime ;
            }

            return $overTime;
        }
       function ConvertTotalLeavesInYearToDays($hours = 0) : Int
       {
        $totalLeavesInDay = intval($hours) / 8;
        return 26 - $totalLeavesInDay;

       }

       function ConvertTotalLeavesInYearToHours($hours )
       {
        if($hours >= 208)
        {
            return 0;
        }else
        {
            return 208 - intval($hours);


        }
       }

       function sumHours($a , $b ){

        if(strlen($a) == 8 && strlen($b) >= 9){
        $a_minutes = substr($a , 3);
        $a_hours = substr($a,0,2);
        $b_minutes = substr($b , 4);
        $b_hours = substr($b , 0,3);
        $SumMinutes = intval($a_minutes) + intval($b_minutes);
        $sumHours = intval($b_hours) + intval($a_hours);
        if($SumMinutes >= 60){
            $sumHours ++ ;
            $SumMinutes = $SumMinutes - 60;
        }
        $glueHoursAndMinutes = $sumHours.':'.$SumMinutes;
        return $glueHoursAndMinutes;
        }

        if(strlen($a) >= 8 && strlen($b) <= 9){
            $a_minutes = substr($a , 3);
            $a_hours = substr($a,0,2);
            $b_minutes = substr($b , 3);
            $b_hours = substr($b , 0,3);
            $SumMinutes = intval($a_minutes) + intval($b_minutes);
            $sumHours = intval($b_hours) + intval($a_hours);
            if($SumMinutes >= 60){
                $sumHours ++ ;
                $SumMinutes = $SumMinutes - 60;
            }
            $glueHoursAndMinutes = $sumHours.':'.$SumMinutes;
            return $glueHoursAndMinutes;
            }

       }


       //Generate Filter PDF
       function filterpdf(Request $request)
       {
        $name = $request->nameFilter;
        $month = $request->month;

        $MainController = new mainController();

        $MainController->generateMypdf($name , $month);

       }





       function filterByClient(Request $request)
       {

           $reqFilter = $request->input('clientReq');
           $user = $request->input('user');
            //$variable = $this->clientpdf('hi');
            if($user == 'همه'){
                $getclient =  DB::table('admins_time')
                    ->orderBy('month','asc')
                    ->where('year' , '=' , $this->year)
                    ->where('client' , '=' , $reqFilter)->paginate(30);

           $getclientsum = DB::table('admins_time')
            ->select('admin_time_id','client_time_id','name','client',DB::raw('SEC_TO_TIME( SUM( TIME_TO_SEC(  CAST(`hours` AS SIGNED)  ) ) ) AS sum'))
            ->groupBy('admin_time_id','client_time_id','name' ,'client')
            ->where('client' , '=' , $reqFilter)
            ->where('year' , '=' , $this->year)
            ->get();

           $getTotalHoursByMonth =
            DB::table('admins_time')
            ->select('admin_time_id','name','month','client',DB::raw('SEC_TO_TIME( SUM( TIME_TO_SEC(  CAST(`hours` AS SIGNED)  ) ) ) AS sum'))
            ->groupBy('admin_time_id','month','name' ,'client')
            ->where('client' , '=' , $reqFilter)
            ->where('year' , '=' , $this->year)
            ->orderBy('month','asc')
            ->get();
            }
            else
            {
                $getclient =  DB::table('admins_time')
                ->orderBy('month','desc')
                ->where('year' , '=' , $this->year)
                ->where('client' , '=' , $reqFilter)->where('name' , '=' , $user)->get();

                $getclientsum = DB::table('admins_time')
                ->select('admin_time_id','client_time_id','name','client',DB::raw('SEC_TO_TIME( SUM( TIME_TO_SEC(  CAST(`hours` AS SIGNED)  ) ) ) AS sum'))
                ->groupBy('admin_time_id','client_time_id','name' ,'client')
                ->where('name','=',$user)
                ->where('year' , '=' , $this->year)
                ->where('client' , '=' , $reqFilter)
                ->get();

                $getTotalHoursByMonth = DB::table('admins_time')
                ->select('admin_time_id','name','month','client',DB::raw('SEC_TO_TIME( SUM( TIME_TO_SEC(  CAST(`hours` AS SIGNED)  ) ) ) AS sum'))
                ->groupBy('admin_time_id','month','name' ,'client')
                ->where('client' , '=' , $reqFilter)
                ->where('year' , '=' , $this->year)
                ->orderBy('month','asc')->where('name','=',$user)
                ->get();
                    }


        //    $getclientsum =  DB::table('admins_time')
        //    ->where('client' , '=' , $reqFilter)
        //    ->sum('hours');

        //$pdf = PDF::loadView('clientFilterpdf' ,compact('getclient','getclientsum','reqFilter','getTotalHoursByMonth','user') );
        //return $pdf->stream('کارت پروژه');
           return view('filterByClient', compact('getclient','getclientsum','reqFilter','getTotalHoursByMonth','user'));

       }

       // Generate Client PDF
       function clientpdf(Request $request){
        $reqFilter = $request->input('clientReq');
        $user = $request->input('user');
         //$variable = $this->clientpdf('hi');
         if($user == 'همه'){
             $getclient =  DB::table('admins_time')
        ->orderBy('month','asc')
        ->where('year' , '=' , $this->year)
        ->where('client' , '=' , $reqFilter)->paginate(30);

        $getclientsum = DB::table('admins_time')
        ->select('admin_time_id','client_time_id','name','client',DB::raw('SEC_TO_TIME( SUM( TIME_TO_SEC(  CAST(`hours` AS SIGNED)  ) ) ) AS sum'))
        ->groupBy('admin_time_id','client_time_id','name' ,'client')
        ->where('year' , '=' , $this->year)
        ->where('client' , '=' , $reqFilter)
        ->get();

        $getTotalHoursByMonth =
        DB::table('admins_time')
        ->select('admin_time_id','name','month','client',DB::raw('SEC_TO_TIME( SUM( TIME_TO_SEC(  CAST(`hours` AS SIGNED)  ) ) ) AS sum'))
        ->groupBy('admin_time_id','month','name' ,'client')
        ->where('client' , '=' , $reqFilter)
        ->where('year' , '=' , $this->year)
        ->orderBy('month','asc')
        ->get();
         }
         else
         {
             $getclient =  DB::table('admins_time')
             ->orderBy('month','desc')
             ->where('client' , '=' , $reqFilter)->where('name' , '=' , $user)->get();

             $getclientsum = DB::table('admins_time')
        ->select('admin_time_id','client_time_id','name','client',DB::raw('SEC_TO_TIME( SUM( TIME_TO_SEC(  CAST(`hours` AS SIGNED)  ) ) ) AS sum'))
        ->groupBy('admin_time_id','client_time_id','name' ,'client')
        ->where('year' , '=' , $this->year)
        ->where('name','=',$user)->where('client' , '=' , $reqFilter)
        ->get();

        $getTotalHoursByMonth = DB::table('admins_time')
        ->select('admin_time_id','name','month','client',DB::raw('SEC_TO_TIME( SUM( TIME_TO_SEC(  CAST(`hours` AS SIGNED)  ) ) ) AS sum'))
        ->groupBy('admin_time_id','month','name' ,'client')
        ->where('client' , '=' , $reqFilter)
        ->where('year' , '=' , $this->year)
        ->orderBy('month','asc')->where('name','=',$user)
        ->get();
         }


     //    $getclientsum =  DB::table('admins_time')
     //    ->where('client' , '=' , $reqFilter)
     //    ->sum('hours');

     $pdf = PDF::loadView('clientFilterpdf' ,compact('getclient','getclientsum','reqFilter','getTotalHoursByMonth','user') );
     return $pdf->stream('کارت پروژه');
       }





}
