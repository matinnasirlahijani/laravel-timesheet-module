<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\api\apiAuthenticationController;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class leaveController extends Controller
{
   public $token = null;
   public $user = null;

    public function __construct(){
        $this->user = apiAuthenticationController::findUserByToken(request()->token);
    }
}
