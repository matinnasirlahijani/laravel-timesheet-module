<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Admin;
use Illuminate\Support\Str;

class apiAuthenticationController extends Controller
{
    function login(Request $request){
        // $request->validate([
        //     'email' => 'required|email',
        //     'password' => 'required'
        // ]);

        $token = Str::random(60);


    $userInfo = Admin::where('email','=', $request->email)->first();
    $credentials = $request->only('email', 'password');

    if (Auth::attempt($credentials)) {
        $userInfo->update(['api_token' => $token]);

        return response($userInfo->api_token, 200);
    }else{
        return response('not Done' , 300);;
    }

    // return response($userInfo , 200);
    }

    function getUserByToken($token)
    {
        $userInfo = Admin::where('api_token','=', $token)->first();

        return response( $userInfo, 200);
    }

    static function findUserByToken($token){

        $userInfo = Admin::where('api_token','=', $token)->first();

        return $userInfo;

    }
}
