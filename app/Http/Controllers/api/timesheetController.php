<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\api\apiAuthenticationController;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\MainController;
use Hekmatinasser\Verta\Verta;
use App\Models\Admin;
use App\Models\time;
use Carbon\Carbon;


class timesheetController extends Controller
{
   public $token = null;
   public $user = null;

    public function __construct(){
        $this->user = apiAuthenticationController::findUserByToken(request()->token);
    }
    function getLatestTimesheet(){

        // $date = Carbon::now()->subDays(60);

        $history = DB::table('admins_time')
            ->where('admin_time_id' ,'=' ,$this->user->id)
            // ->where('created_at' , '>=' , Carbon::now()->subDays(60) )
            ->orderBy('geregorian' , 'desc')
            ->get();

        return response($history, 200);
    }

    function addTimesheet(Request $request, MainController $mainController){
        $lunch = 0;
        $duty = 0;

        if($request->lunch == true)
            {
                //dd('yes');
                $lunch = 1;
            }

            if($request->duty == true)
            {
                //dd('yes');
                $duty = 1;
            }
            $comment = $request->comment;
            if(!$request->comment)
            {
                $comment = 'ندارد';
            }
            //Add Year
            $verta = new Verta;
            $year = $verta->year;
            $clientname = $request->client;
            //Get the User ID
            $userInfo = Admin::where('api_token','=', $request->token)->first();
            // session()->put('UserName' , $userInfo->name);
            // $user = session();
           //Calculate Diff Between Time-in and Time-out
           $time_in = $request->time_in;
           $time_out = $request->time_out;
           $isFriday = $request->date;
           $time_dif = $mainController->timediff($time_in, $time_out ,$isFriday);
           if($lunch){
            $carbon = new Carbon($time_dif);
            $time_dif = ($carbon->subMinutes(30)->format('H:i:s'));
            // dd($time_dif);
            // die();
           }


           //Shmasi To Miladi Conversion
           $getMiladiDate = $request->date;
           $dateExplode = (explode(" ",$request->date));




           $sortedArray = $mainController->TrimShamsiDateToMiladiDate($dateExplode);
           $getEnLocale = $mainController->FaToEnLocale($sortedArray[2],$sortedArray[1],$sortedArray[0]);
           $geregorian = implode("/",Verta::getGregorian($getEnLocale[0],$getEnLocale[1],$getEnLocale[2]));




           $clientsheet = DB::table('clientsheets')->get();

            $timesheet = new time;
            $timesheet->client = $request->client;
            $timesheet->fiscalYear = $request->fiscalYear;
            $timesheet->time_in = $request->time_in;
            $timesheet->time_out = $request->time_out;
            $timesheet->service = $request->service;
            $timesheet->task = $request->task;
            $timesheet->month = $request->month;
            $timesheet->duty = $duty;
            $timesheet->lunch = $lunch;
            $timesheet->year = $year;
            $timesheet->hours = $time_dif;
            $timesheet->date = $request->date;
            $timesheet->geregorian = $geregorian;
            $timesheet->admin_time_id = $request->token;
            $timesheet->name = $userInfo->name;
            $timesheet->comments = $comment;
            $getclientid = DB::table('clientsheets')->where('clientName' ,'=' , $clientname)->first();

            //foreach ($getclientid as $getid)
            //{
                //echo(". $getid->id");
           // }

            $timesheet->client_time_id = $getclientid->id;
        //    dd($getclientid);
           // dd($userInfo->id);
            // $timesheet->save();

           // event(new newrecord(Request()->all(),Session::all()));
          // $reqall = Request()->all();
         //$this->dispatch(new MailJob($reqall));
        // echo($time_dif)->format('%H:%I ');

             return response($lunch, 200);
    }
}
