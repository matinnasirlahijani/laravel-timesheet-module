<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class clientsController extends Controller
{
    function getClients(){
        $clients = DB::table('clientsheets')->get();
        return response($clients, 200);
    }
}
