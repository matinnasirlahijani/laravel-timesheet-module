<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Admin;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use Verta;
class AuthCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $admin = new Admin;
        if(!session()->has('LoggedUser') && $request->path() == 'add'){
            return redirect('tlogin');
        }

        // if(!session()->has('LoggedUser') && $request->path() == 'reg'){
        //     return redirect('tlogin');
        // }

        if(!session()->has('LoggedUser') && $request->path() == 'tdashboard' ){
            return redirect('tlogin');
        }
            if(session()->has('is_admin', '=' , '1')){
                return redirect('add');
            }
            if(session()->has('LoggedUser') && $request->path()=='tlogin' || $request->path()=='/'){
                return redirect('tdashboard');
            }

            // if(session()->has('LoggedUser')){
            //     $mytime = new Verta();
            //     Storage::append('log.txt',  session::get('UserName').' Logged in'.' '.'@'.' '.$mytime);
            // }





            // }else{
            //     return redirect('tlogin');
            // }
       //}

            // if(session()->has('LoggedUser') && session()->has('LoggedUserInfo'->admin::where('is_admin' , '=' , '1'))){
            //     return redirect('add');
            // }




        return $next($request)->header('Cache-Control','no-cache , no-store , max-age=0, must-revalidate')
        ->header('Pragma', 'no-cache')
        ->header('Expires' , 'Sat 01 Jan 1990 00:00:00 GMT');
    }
}
