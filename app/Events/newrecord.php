<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Bus\Queueable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Support\Facades\Session;
use Illuminate\Queue\SerializesModels;

class newrecord implements ShouldQueue
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    
    public $data;
    public $uname ;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($data,$uname)
    {
        $this->data = Request()->all() ;
        $this->uname = session::get('UserName');
        
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        
        return new PrivateChannel('channel-name');
    }
}
