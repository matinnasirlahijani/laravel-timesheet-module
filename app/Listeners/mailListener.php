<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\recordinfo;
use App\Events\newrecord;
use App\Models\Admin;
use Illuminate\Support\Facades\Session;
use Mail;
class mailListener implements ShouldQueue
{
    
    public $connection = 'database';
    public $queue = 'mailqueue';
    public $delay = 10;
    
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(newrecord $event)
    {
        $a = array($event);
        Mail::send('mail.newrecord', ['event' => $a], function($message) use ($event) {
            $message->to('matinnasirlahijani@gmail.com');
            $message->subject("رکورد جدیدی ثبت شد");
        });
            
    }
}
