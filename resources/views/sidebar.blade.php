<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sidebar</title>
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}
    {{--<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.0/font/bootstrap-icons.css">--}}
    <link rel="stylesheet" href="{{asset('bootstrap-icons/font/bootstrap-icons.css')}}" >
    {{-- <link href="{{asset('fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css"> --}}
    {{-- <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet"> --}}

    <!-- Custom styles for this template-->
    <link href="{{asset('css/sb-admin-2.css')}}" rel="stylesheet">
    <link href="{{asset('css/sidebar.css')}}" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css" rel="stylesheet">
    <script src="{{ asset('js/sidebar.js') }}"></script>
<?php
use App\Models\Admin;
     use App\Models\time;
     $admins = Admin::all();
     $times = time::all();
?>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style type="text/css">

    /* i { color: blue !important} */
    i:hover { color: #f0f5fc !important; }


        </style>
        <script>
            // var nav = document.getElementById('#nav');
            $(document).ready(function(){
                $(".nav").hover(function(){
                    // $("#toggleIcon").toggleClass('bi bi-layout-sidebar-inset');
                    // $("#accordionSidebar").toggle();
                    $(".l-navbar").toggleClass('show')
                    $('#body-pd').toggleClass('body-pd')
                    $('#header').toggleClass('body-pd')
  });



                    $("#sidebarToggleSecond").click(function(){
                    $("#sidebarToggleSecond").toggleClass('bi bi-arrow-left-circle');

  });

});


</script>
</head>
{{-- <body style="direction: rtl"> --}}
    <body id="body-pd" class="wrapper" style="direction: rtl">
        @yield('sidebar')
        <header class="header" id="header">
            <div class="header_toggle"> <i class='bx bx-menu' id="header-toggle"></i> </div>
            <div class="header-txt"><span>{{ Session::get('UserName') }}</span></div>
            <div class="header_img"><img src="{{asset('images/undraw_profile_2.svg')}}" alt=""> </div>
        </header>
        <div class="l-navbar" id="nav-bar">
            <nav class="nav" >
                <div>
                    <div class="nav_list">
                        @if (Session::get('is_admin'))
                        <a href="{{route('tdashboard')}}" class="{{ url()->current() == route('tdashboard') ? 'nav_link active' : 'nav_link' }}">
                            <i class='bx bx-grid-alt nav_icon'></i>
                            <span class="nav_name">داشبورد</span>
                        </a>@endif
                            <a href="{{route('add')}}" class="{{ url()->current() == route('add') ? 'nav_link active' : 'nav_link' }}">
                                <i class='bx bx-plus nav_icon'></i>
                                <span class="nav_name">افزودن</span> </a>
                                <a href="{{route('leave')}}" class="{{ url()->current() == route('leave') ? 'nav_link active' : 'nav_link' }}">
                                    <i class='bx bx-bed nav_icon'></i>
                                    <span class="nav_name">مرخصی</span> </a>
                                    @if (Session::get('is_admin'))
                                    <a href="{{route('addclient')}}" class="{{ url()->current() == route('addclient') ? 'nav_link active' : 'nav_link' }}">
                                        <i class='bx bx-user-plus nav_icon'></i>
                                        <span class="nav_name">مشتری ها</span> </a>
                                        @endif
                                        {{-- <a href="#" class="nav_link">
                                            <i class='bx bx-folder nav_icon'></i>
                                            <span class="nav_name">Files</span> </a>
                                            <a href="#" class="nav_link">
                                                <i class='bx bx-bar-chart-alt-2 nav_icon'></i>
                                                <span class="nav_name">Stats</span> </a>  --}}
                                            </div>
                </div>
                <a href="{{ route('logout') }}" class="nav_link">
                    <i class='bx bx-log-out nav_icon'></i>
                    <span class="nav_name">خروج</span> </a>
            </nav>
        </div>
        <!--Container Main start-->
        {{-- <div class="height-100 bg-light">
            <h4>Main Components</h4>
        </div> --}}
</body>
</html>
