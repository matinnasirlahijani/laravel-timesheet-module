<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>تایم شیت - افزودن</title>
    <link rel="icon" href="{{asset('images/logo-80x80.jpg')}}" type="image/x-icon">
    <?php
    use Illuminate\Support\Str;
  ?>

    <!-- Scripts -->
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}

    <!-- Styles -->
    {{-- <script src="https://code.jquery.com/jquery-3.6.0.js"></script> --}}
    {{-- <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script> --}}
    <script src="{{asset('jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('jquery-ui/jquery-ui-1.13.2/jquery-ui.min.js')}}"></script>
    <script src="{{asset('jquery-ui/jquery-ui-1.13.2/jquery-ui.min.css')}}"></script>
    <script src="{{asset('select2/select2.js')}}"></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{asset('select2/select2.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('DatePicker/dist/css/persian-datepicker.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('Toastify/example/script.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Toastify/toastify.css')}}">
  <script src="{{ asset('DatePicker/assets/persian-date.min.js') }}"></script>
  <script src="{{ asset('DatePicker/dist/js/persian-datepicker.js') }}"></script>
  <script>


    //Get Current Month
    var monthName = new persianDate().format('MMMM');
   // console.log(new persianDate().format('MMMM'));


    $( function() {
      $( "#datepicker" ).datepicker();
      $('.normal-example').persianDatepicker({
        maxDate: new persianDate().add('day', 30).valueOf(),
        minDate: new persianDate().subtract('day', 40).valueOf(),
        format:'dddd D MMMM YYYY',
        autoClose: true



      });



    } );







    $(function(){


      $('#only-timepicker').datepicker();

      //console.log(new persianDate().toLocale('en').format('YY'));
        $('.only-timepicker-example').persianDatepicker({
          onlyTimePicker: true,
       //format: new persianDate('.only-timepicker-example').toLocale('en').format('H:m') ,
        format: 'H:m',
        calendar:{
        persian: {
            locale: 'en'
        }
    }

});
      });


    $(function() {
    $('#taskdiv').hide();
    $('#service').change(function(){
        if($('#service').val() == 'حسابرسی') {
            $('#taskdiv').show();
        } else {
            $('#taskdiv').hide();
        }
    });
});

$(function () {
  $(".select2").select2();
});

$(document).ready(function () {

  // On button click, get value
  // of input control Show alert
  // message box

      var inputString = $("#month").val(monthName);
      //alert(inputString);
      //inputString.value = monthName;

});


    </script>
</head>
<style>
    body{
        overflow: scroll !important;
    }
</style>


<body class="">


        @include('sidebar')
        @section('sidebar')




        <!-- Content -->
        <div class="">

        </div>
    <div class="col mt-8">

      @if (session('added'))
      <div class="col-sm-5">
          <div class="alert alert-success text-right hidden">
            <div href="#" id="new-timesheet" class="hidden">{{(session('added'))}}</div>

          </div>
        </div>
      @endif

      @if (session('Delete-Done'))
      <div class="col-sm-5">
          {{-- <div class="pl-5 pr-5 alert alert-success text-right">
              {{ session('Delete-Done') }}
          </div> --}}
          <div class="pl-5 pr-5 alert alert-success text-right hidden">
            <div href="#" id="timeDelete" class="">{{(session('Delete-Done'))}}</div>
          </div>
        </div>
      @endif


<div class=" d-block text-center fs-2"  >
  {{-- <p style = "font-size:1em" class="badge px-3 py-3 mx-3 fs-2 bg-primary text-white rounded"> سلام {{Session::get('UserName')}} <i class="bi bi-person-fill" style="font-size: 1.1rem;"></i> </p> --}}
</div>
<div class="form-outline d-block text-right " >
<div class='col-sm-10 col-md-12 col-lg-15 d-block col-sm-9 px-sm-10  '>
<?php
        use App\Models\clientsheet;
        $clientsheets = Clientsheet::all();
    ?>

   {{-- <form method="post" name="shhowPDF" action="{{route('downloadMyPDF')}}" method="POST">
    @csrf
    <div class="row">
    <div class="col col-md-4 inline-block">

    <select class="form-control "  name="month" >
      <option selected >  گزینه ای وارد کنید</option>
      <option  >همه ماه ها</option>
      <option  >فروردین</option>
      <option>اردیبهشت</option>
      <option>خرداد</option>
      <option>تیر</option>
      <option>مرداد</option>
      <option>شهریور</option>
      <option>مهر</option>
      <option>آبان</option>
      <option>آذر</option>
      <option>دی</option>
      <option>بهمن</option>
      <option>اسفند</option>
    </select>
    <label class=" form-lable"> ماه گزارشگیری pdf</label>
    <div class="col col-md-8">
    <button class="btn btn-success" type="submit">دانلود pdf</button>

    {{-- <button class="btn btn-success">دانلود PDF</button> --}}
    {{-- <div class="row">

       </div>
</div>

    </div>

   </form>  --}}


<div class="row my-2 col col-lg-12 col-sm-12 col-xsm-12 mt-2" style="" id="navbar_top">




    <form action="{{ route('added') }}" method="post" name="add" class="bg-gray mt-10 p-2 rounded-md" style="">
        <br>
        <p class="block">
         <hr style="width:50%", size="3", color=black>
     </p>
      @csrf
    <div class="row">



<div class="col col-sm-12 col-md-4 ">
    <select id="inputfilter select2" name="client" class="form-control select2">
        {{-- <option selected>انتخاب کنید</option> --}}
        @foreach($clientsheets as $clientsh)
        <option selected>{{ $clientsh['clientName'] }}</option>
        @endforeach
      </select>
      <label class="form-label">مشتری</label>



  </div>
  <div class="col-md-2 ">
    <select  id="form1 fiscalYear" name="fiscalYear" class="form-control">
      <option selected value="97">97</option>
      <option>98</option>
      <option>99</option>
      <option>1400</option>
      <option>1401</option>
      <option>1402</option>
      <option>1403</option>
      <option>1404</option>
      <option>1405</option>

  </select>
  <label class="form-label"> سال مالی</label></div>

    <div class="col-md-2">
      <select  id="service" name="service" class="form-control">
        <option selected>حسابداری</option>
        <option>حسابرسی</option>
  </select>
  <label class="form-label" for="form1">خدمات</label>
</div>


  <div class="col-md-4" id="taskdiv">
    <select  id="form1 select2" name="task" class="form-control select2" style="width: 100%">
      <option selected>  ندارد  </option>
      <option >A - گزارشهای حسابرسی و صورتهای مالی</option>
      <option>B - چک لیست ها</option>
      <option>C - برنامه ریزی کلی و تعیین استراتژی حسابرسی</option>
      <option>D - تأییدیه ها و مکاتبات</option>
      <option>E - برنامه ریزی و ارزیابی آزمون کنترلها</option>
      <option>F - آزمون کنترلها</option>
      <option>G - برنامه ریزی و ارزیابی آزمون محتوا</option>
      <option>H - موجودی نقد و بانک</option>
      <option>I - سرمایه گذاریها</option>
      <option>J - حسابها و اسناد دریافتنی تجاری</option>
      <option>K - سایر حسابها و اسناد دریافتنی</option>
      <option>L - موجودی کالا</option>
      <option>M - پیش پرداختها، سفارشات و سپرده ها</option>
      <option>N - دارایی های ثابت مشهود</option>
      <option>O - دارایی های نامشهود و سایر دارایی ها</option>
      <option>P - حسابها و اسناد پرداختنی</option>
      <option>Q - سایر حسابها و اسناد پرداختنی</option>
      <option>R - پیش دریافتها</option>
      <option>S - مالیات بر عملکرد</option>
      <option>T - تسهیلات مالی دریافتی</option>
      <option>U - حقوق صاحبان سهام</option>
      <option>V - فروش کالا و خدمات</option>
      <option>W - بهای تمام شده</option>
      <option>X - هزینه های تولیدی، خدماتی، اداری، فروش و مالی</option>
      <option>Y - سایر درآمد ها و هزینه های عملیاتی و غیر عملیاتی </option>
      <option>Z - تراز آزمایشی / حسابهای انتظامی / دفاتر قانونی</option>
      <option>A-Z - پرونده دایم</option>

    </select>
    <label class="form-label" for="task">  تخصیص کار</label>
  </div>

  <div class="col-md-4 " id="taskdiv" class="form-control">
    <select  id="month" name="month" class="form-control">
      <option selected> گزینه ای وارد کنید</option>
      <option >فروردین</option>
      <option>اردیبهشت</option>
      <option>خرداد</option>
      <option>تیر</option>
      <option>مرداد</option>
      <option>شهریور</option>
      <option>مهر</option>
      <option>آبان</option>
      <option>آذر</option>
      <option>دی</option>
      <option>بهمن</option>
      <option>اسفند</option>
    </select>
    <label> ماه </label>
  </div>

    <div class="col-md-2">
        <input name="time_in" style="background-color: #fff" class="only-timepicker-example form-control" id="only-timepicker"/>
        <label class="form-label" for="">ساعت ورود</label>
    </div>

    <div class="col-md-2">
        <input type="text" id="" name="time_out"  style="background-color: #fff" class="only-timepicker-example form-control" id="only-timepicker" />
        <label class="form-label" for="">ساعت خروج</label>
    </div>

  <div class="col-md-1 form-check form-check-inline">
    <input type="checkbox" id="duty" name="duty" class="form-check-input p-2" />
    <label class="form-check-lable" for="duty">مأموریت</label>

  </div>

  <div class="col-md-1 form-check form-check-inline">
    <input type="checkbox" id="lunchTime" name="lunch" class="form-check-input p-2" />
    <label class="form-check-lable" for="lunchTime">نهاری</label>

  </div>
  {{-- <div class="col">
    <input type="text" id="" name="hours" class="form-control" />
    <label class="form-label" for="">به مدت (ساعت)</label>
  </div> --}}

  <div class="col-md-3">
     <input id="inlineExample form1" readonly style="background-color: #fff" name="date" class="normal-example form-control" />
     <label class="form-label"  for="form1">مورخ</label>
  </div>

  <div class="col-md-3">
    <label > توضیحات (اختیاری) </label>
    <textarea name="comment" value="ندارد"> </textarea>
  </div>

  <div class="col"></div>


</div>
  <br>
  <button type="submit" class="btn btn-primary p-2"> ذخیره</button>
</div>

{{-- <div class="alert alert-warning my-3 mx-5" role="alert">
  <i class="bi bi-bell-fill"></i>
  توجه!
  تایم شیت ذخیره شده تنها به مدت ۳ روز قابل مشاهده و ویرایش است لذا در صورتی که نیاز به ویرایش تایم شیت خاصی است ظرف مدت ۳ روز به بروزرسانی آن اقدام نمایید در غیر اینصورت امکان ویرایش تایم شیت وجود ندارد.
</div> --}}
</form>

<div class="mt-3 mr-5 pt-6">
    {{-- Download PDF   --}}

    <form method="post" name="shhowPDF" action="{{route('downloadMyPDF')}}" method="POST">
        @csrf
        <div class="row">
        <div class="col col-md-4 inline-block">

        <select class="form-control "  name="month" >
          <option selected value='گزینه ای وارد کنید' >  گزینه ای وارد کنید</option>
          <option  >همه ماه ها</option>
          <option  >فروردین</option>
          <option>اردیبهشت</option>
          <option>خرداد</option>
          <option>تیر</option>
          <option>مرداد</option>
          <option>شهریور</option>
          <option>مهر</option>
          <option>آبان</option>
          <option>آذر</option>
          <option>دی</option>
          <option>بهمن</option>
          <option>اسفند</option>
        </select>
        <label class=" form-lable"> ماه گزارشگیری pdf</label>
        <div class="col col-md-8">
        <button class="btn btn-success" type="submit">دانلود
            <i class="bi bi-cloud-arrow-down-fill" style="font-size:  1.1rem;"></i></button>

        {{-- <button class="btn btn-success">دانلود PDF</button> --}}
         <div class="row">

           </div>
    </div>

        </div>

       </form>



     {{--  End Download PDF     --}}
</div>
</div>
  </div>
  <div class="pt-5 mt-5"></div>
  {{-- <div class="pt-5 mt-5"></div>
  <div class="pt-5 mt-5"></div> --}}
  <div class="text-success text-right">
    <h3 class="text-right"> ویرایش تایم شیت </h3>
  </div>
  <div class="col-sm-12">
  <table class="table table-responsive table-striped border-2 mt-8 ">
    <th> </th>
    <th> شناسه </th>
    <th> مشتری</th>
    <th> سال مالی</th>
    <th> خدمات </th>
    <th> تخصیص </th>
    <th> ماه </th>
    <th> ماموریت </th>
    <th> ساعت ورود </th>
    <th> ساعت خروج </th>
    <th>  نهاری </th>
    <th> مورخ </th>
    <th> حذف </th>




  <div>
    @foreach ($history as $myHistory)
      <tr>
        <td> <a href="{{route('editMyTimeSheet' , ['id' => $myHistory->id]) }}"> ویرایش</a> </td>
        <td scoope="col"> {{$myHistory->id}} </td>
        <td scoope="col"> {{$myHistory->client}} </td>
        <td scoope="col"> {{$myHistory->fiscalYear}} </td>
        <td scoope="col"> {{$myHistory->service}} </td>
        <td scoope="col"> {{$myHistory->task}} </td>
        <td scoope="col"> {{$myHistory->month}} </td>
    @if ($myHistory->duty == 1)
        <td scope="col">دارد</td>
    @endif
    @if ($myHistory->duty == 0)
        <td scope="col">ندارد</td>
    @endif
        <td scoope="col"> {{ str::limit($myHistory->time_in,5,'') }} </td>
        <td scoope="col"> {{ str::limit($myHistory->time_out,5,'') }} </td>
        @if ($myHistory->lunch == 1)
        <td scope="col">دارد</td>
    @endif
    @if ($myHistory->lunch == 0)
        <td scope="col">ندارد</td>
    @endif
        <td scoope="col"> {{$myHistory->date}} </td>
        <td scoope="col"> <a class="btn btn-danger" href="{{route('deleteMyTimesheet',['id' => $myHistory->id])}}"> حذف</a>  </td>

      </tr>
    @endforeach
  </div>

  </table>
</div>
</div>
</div>

</body>
<script type="text/javascript" src="{{asset('Toastify/toastify.js')}}"> </script>
<script type="text/javascript" src="{{asset('Toastify/example/script.js')}}"> </script>
<script>
 document.addEventListener("DOMContentLoaded", function (event) {
    var scrollpos = localStorage.getItem("scrollpos");
    if (scrollpos) window.scrollTo(0, scrollpos);
  });

  window.onscroll = function (e) {
    localStorage.setItem("scrollpos", window.scrollY);
  };

  var monthPicker = document.getElementById("#fiscalYear");
    console.log(monthPicker);


</script>
</html>
