<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ویرایش مرخصی</title>
    <link rel="icon" href="{{asset('images/logo-80x80.jpg')}}" type="image/x-icon">
    {{-- <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script> --}}
    <script src="{{asset('jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('jquery-ui/jquery-ui-1.13.2/jquery-ui.js')}}"></script>
    <link rel="stylesheet" href="{{ asset('DatePicker/dist/css/persian-datepicker.css') }}"/>
    <script src="{{ asset('DatePicker/assets/persian-date.min.js') }}"></script>
    <script src="{{ asset('DatePicker/dist/js/persian-datepicker.js') }}"></script>
    <script>



      $( function() {
        $( "#datepicker" ).datepicker();
        $('.normal-example').persianDatepicker({
          autoClose: true,
          initialValue: false,
        //   maxDate: new persianDate().add('day', 0).valueOf(),
        //   minDate: new persianDate().subtract('day', 2).valueOf(),
          format:'dddd D MMMM YYYY',


        });




      } );


      $(function(){


        $('#only-timepicker').datepicker();

        //console.log(new persianDate().toLocale('en').format('YY'));
          $('.only-timepicker-example').persianDatepicker({
            onlyTimePicker: true,
         //format: new persianDate('.only-timepicker-example').toLocale('en').format('H:m') ,
          format: 'H:m',
          calendar:{
          persian: {
              locale: 'en'
          }
      }

  });
        });




      </script>
</head>
<body>
    @include('sidebar')
    @section('sidebar')
        <!-- Content -->
        <div class="">

        </div>
    <div class="col py-3 mt-8">

        @if (session('leaveUpdated'))
        <div class="col-sm-5">
            <div class="pl-5 pr-5 alert alert-success text-right">
                {{ session('leaveUpdated') }}
            </div>
          </div>
        @endif

        <div class=" d-block text-center fs-2"  >
            <p style = "font-size:1em" class="badge px-3 py-3 mx-3 fs-2 bg-success text-white rounded"> ویرایش مرخصی  </p>
          </div>

        <div class="form-outline d-block text-right " >
            <div class='col-sm-10 col-md-12 col-lg-12 d-block col-sm-9 px-sm-10  '>


                <form action="{{ route('updateMyLeave') }}" method="post"
                 name="updateMyLeave" class="">
                  @csrf
                {{-- <div class="row"> --}}
                    @foreach ($FindLeaveById as $myLeavs )


              <div class="col col-md-2 col-sm-12 col-xsm-12 col-lg-2">
                <input name="time_in" readonly style="background-color: #fff" class="only-timepicker-example form-control"
                 id="only-timepicker" value="{{$myLeavs->time_in}}"/>
                <label class="form-label" for=""> از ساعت</label>
              </div>

              <div class="col col-md-2 col-sm-12 col-xsm-12 col-lg-2">
                <input type="text" id="" readonly style="background-color: #fff" name="time_out" class="only-timepicker-example form-control"
                 id="only-timepicker" value="{{$myLeavs->time_out}}" />
                <label class="form-label" for="">تا ساعت </label>
              </div>
              {{-- <div class="col">
                <input type="text" id="" name="hours" class="form-control" />
                <label class="form-label" for="">به مدت (ساعت)</label>
              </div> --}}
              <div class="col col-md-3 col-sm-12 col-xsm-12 col-lg-2" id="">
                <select  id="form1" name="month" class="form-control">
                  <option selected> {{$myLeavs->month}}</option>
                  <option >فروردین</option>
                  <option>اردیبهشت</option>
                  <option>خرداد</option>
                  <option>تیر</option>
                  <option>مرداد</option>
                  <option>شهریور</option>
                  <option>مهر</option>
                  <option>آبان</option>
                  <option>آذر</option>
                  <option>دی</option>
                  <option>بهمن</option>
                  <option>اسفند</option>
                </select>
                <label>  در ماه </label>
              </div>
              <div class="col col-md-3 col-sm-12 col-xsm-12 col-lg-2">
                 <input id="inlineExample form1 datepicker" name="date" readonly style="background-color: #fff"
                  class="normal-example form-control" value="{{$myLeavs->date}}" />
                 <label class="form-label" for="form1">مورخ</label>
              </div>
              @endforeach
              <div class="col"></div>
                <input type="hidden" name="id" value="{{$id}}"/>
                {{-- <div class="inline-example" ></div> --}}
            {{-- </div> --}}
              <br>
            </div>
            <button type="submit" class="btn btn-primary"> ذخیره </button>
            </form>
              </div>

    </div>
</body>
</html>
