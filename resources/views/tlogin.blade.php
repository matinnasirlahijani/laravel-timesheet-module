<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>تایم شیت - ورود</title>
    <link rel="icon" href="{{asset('images/logo.jpg')}}" type="image/x-icon">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link href="{{asset('fontawesome-free/css/all.css')}}" rel="stylesheet" type="text/css">
    {{-- <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet"> --}}

    <!-- Custom styles for this template-->
    <link href="{{asset('css/sb-admin-2.min.css')}}" rel="stylesheet">
</head>

{{-- <body class="">
<div class=" vertical-center">
    <div class="container py-5">
        <div class="row ml-auto mx-auto">


                <img src="{{asset('images/logo.jpg')}}" class="center-block mx-auto" alt="Logo">


        </div>
    <div class="row">
        <div class='col-md-3 col-sm-1'></div>
        <div class="col-md-6">
            <div class="login-box well">
                    <form action="{{ route('check') }}" method="post">
                        @if(Session::get('fail'))
                        <div class="alert alert-danger">

                        {{Session::get('fail')}}
                        </div>
                        @endif
                        @csrf
                        <legend class="text-right">ورود</legend>
                        <div class="form-group text-right">
                            <label for="username-email" class="text-right">ایمیل</label>
                            <input value='' id="username-email" placeholder="Example@gmail.com" name="email" type="text" class="form-control" />
                            <span class="text-danger">@error('email'){{ $message }} @enderror</span>
                        </div>
                        <div class="form-group text-right">
                            <label for="password" class="text-right">گذرواژه</label>
                            <input id="password" value='' placeholder="****" name="password" type="password" class="form-control" />
                            <span class="text-danger">@error('password'){{$message}} @enderror</span>
                        </div>
                        <div class="input-group">
                          <div class="checkbox">
                            <label>
                              <input id="login-remember" type="checkbox" name="remember" value="1"> مرا به خاطر بسپار
                            </label>
                          </div>
                        </div>
                        <div class="form-group text-right">
                            <button type="submit" class="btn btn-primary text-right" value="Login">ورود</button>
                        </div>


                    </form>
</div>
            </div>
        </div>

    </div>
</div>
<div class="d-flex justify-content-center">
    <div class="col col-md-6 text-center">

        <a class="text-center" href="https://www.linkedin.com/in/matt-lahijani-48858b113/">
            <h6 class="text-center">طراحی و توسعه توسط متین لاهیجانی</h6>
        </a>
    </div>
</div>
</body> --}}



<body class="bg-gradient-primary" style="direction: rtl;">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center mx-0">

            <div class="col-xl-10 col-lg-12 col-md-9 ">

                <div class="card o-hidden border-0 shadow-lg my-5 ">
                    <div class="card-body p-0 text-center">
                        <!-- Nested Row within Card Body -->
                        <div class="text-center">
                            <div class=" d-block d-lg-block ">
                                <img src="{{asset('images/logo.jpg')}}" alt="" width="253px" height="210px">
                                <hr>
                            </div>
                            <div class="col-lg-6 text-center">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">موسسه بینا تدبیر</h1>
                                    </div>
                                    <div class="text-center">
                                <form class="text-center " action="{{ route('check') }}" method="post">
                                    @csrf

                                    @if(Session::get('fail'))
                                    <div class="alert alert-danger">

                                    {{Session::get('fail')}}
                                    </div>
                                    @endif
                                        <div class="form-group">
                                            <input type="email" class="form-control form-control-user"
                                                aria-describedby="emailHelp"
                                                value='' id="username-email"
                                                name="email"
                                                placeholder="ایمیل">

                                                <span class="text-danger">@error('email'){{ $message }} @enderror</span>
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control form-control-user" name="password"
                                                 placeholder="پسورد" id="password" value=''>

                                                <span class="text-danger">@error('password'){{$message}} @enderror</span>
                                        </div>
                                        {{-- <div class="form-group"> --}}
                                            {{-- <div class="custom-control custom-checkbox small">
                                                <input type="checkbox" class="custom-control-input" id="customCheck">
                                                <label class="custom-control-label" for="customCheck">مرا به خاطر بسپار</label>
                                            </div> --}}
                                        {{-- </div> --}}
                                        <button type="submit" class="btn btn-primary btn-user btn-block">
                                            ورود
                                        </button>
                                        {{-- <hr> --}}
                                        {{-- <a href="index.html" class="btn btn-google btn-user btn-block">
                                            <i class="fab fa-google fa-fw"></i> Login with Google
                                        </a>
                                        <a href="index.html" class="btn btn-facebook btn-user btn-block">
                                            <i class="fab fa-facebook-f fa-fw"></i> Login with Facebook
                                        </a>
                                        <hr>
                                        <div class="text-center">
                                            <a class="small" href="forgot-password.html">Forgot Password?</a>
                                        </div>
                                        <div class="text-center">
                                            <a class="small" href="register.html">Create an Account!</a>
                                        </div> --}}
                                    </form>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

</body>
</html>
