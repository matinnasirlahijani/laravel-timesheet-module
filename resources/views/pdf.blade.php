<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>گزارش ۳۰ روز گذشته</title>
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}

    <!-- Styles -->
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
</head>

</head>
<style>
body {
	font-family: 'examplefont', sans-serif;
  direction: rtl;
}
.btn-success {
  color: #fff;
  background-color: #38c172;
  border-color: #38c172;
}
p{
  display: inline;
  font-size: 25px;
  color:navy;
  margin: 5%;
}
table{
  border: 1px solid black;
  border-color:black;
  border-collapse: collapse;
  width: 100%;
}
td{
  padding-left: 5%;
  height: 10%;
  border-bottom: solid 1px black;
  border-right: solid 1px black;
  border-collapse: collapse;
}

tr:nth-child(even) {
  background-color: #add8e6;
}
h1{
  padding-bottom: 5%;
}
@page {
	header: page-header;
  footer: page-footer;
}
.center{
  padding-left: 50%;
}


    </style>
<body>
  <header class="h">
  <htmlpageheader name="page-header">
    
<h1 class="header"> گزارش ۳۰ روز گذشته</h1>

    </htmlpageheader>
  </header>
 <br>
 <br>
     
     <table>
      <tr>
        <td>
          <p> کاربر</p>      
        </td>
        <td>
          <p> مشتری</p>      
        </td>
        <td>
          <p> سرویس</p>      
        </td>
        <td>
          <p> ساعت</p>      
        </td>

        <td>
          <p> تاریخ</p>      
        </td>
      </tr>

      
         @foreach ($lastMonth as $ab)
         <tr>
         <td>
           {{ $ab->name}}     
        </td>
        <td>
           {{ $ab->client }}    
        </td>
        <td>
          {{ $ab->service }}
               
        </td>
        <td>
          {{ $ab->hours }}
              
        </td>

        <td>
          {{ $ab->date }}    
        </td>
    

      </tr>
      @endforeach
      
     </table>
     <footer>
     <htmlpagefooter name="page-footer" class="footer">
      صفحه
      {PAGENO}
    </htmlpagefooter>
     </footer>
      
           
       
</body>
</html>