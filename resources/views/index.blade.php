<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <?php
            use App\Models\Admin;
            use Illuminate\Support\Facades\DB;
            $a = DB::table('admins')->paginate(2);
            ?>
            @foreach ($a as $ab)
            {{$ab->name}}
            @endforeach
        </div>
        <a href="{{ $a->nextPageUrl() }}">
            Next
        </a>

        <a href="{{ $a->previousPageUrl() }}">
            Pre
        </a>
        
    </div>
</body>
</html>