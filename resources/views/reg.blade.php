<!doctype html>
<html lang="">
<head>
    <meta charset="utf-8">

    <title>عضویت کاربر جدید</title>
    <link rel="icon" href="{{asset('images/logo-80x80.jpg')}}" type="image/x-icon">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body class="">
<div class="container text-center py-5 my-5">
<form class="form-horizontal" action="{{ route('save') }}" method="get">
   {{--  @if($res == 'yes') 
    <div class="alert alert-success" role="alert">
    {{ $res  }}
    </div>
     @endif 
    
   @if($res == 'no')
    <div class="alert alert-danger" role="alert">
    {{ $res }}
</div>
    @endif --}}
   
    @csrf
  <fieldset>
    <div id="legend">
      <legend class="">کاربر جدید</legend>
    </div>
    <div class="control-group">
      <!-- Username -->
      
      <div class="controls">
        
      </div>
    </div>
    <div class="control-group">
      <!-- Username -->
      <label class="control-label"  for="username">نام:</label>
      <div class="controls">
        <input type="text" id="username" name="name" placeholder="" class="input-xlarge">
       
      </div>
    </div>
 
    <div class="control-group">
      <!-- E-mail -->
      <label class="control-label" for="email">ایمیل:</label>
      <div class="controls">
        <input type="text" id="email" name="email" placeholder="" class="input-xlarge">
       
      </div>
    </div>
 
    <div class="control-group">
      <!-- Password-->
      <label class="control-label" for="password">پسورد:</label>
      <div class="controls">
        <input type="password" id="password" name="password" placeholder="" class="input-xlarge">
       
      </div>
    </div>
 
    <div class="control-group">
      <!-- Password -->
      <label class="control-label"  for="password_confirm">تایید پسورد:</label>
      <div class="controls">
        <input type="password" id="password_confirm" name="password_confirm" placeholder="" class="input-xlarge">
        
      </div>
    </div>
 
    
      <!-- Button -->
      <div class="controls py-5">
        <button type="submit" class="btn btn-success">عضو شوید</button>
      </div>
    </div>
  </fieldset>
</form>
</div>
<body>
    <html>