<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>مشتری</title>
    <link rel="icon" href="{{asset('images/logo-80x80.jpg')}}" type="image/x-icon">

</head>
<body class="">
    @include('sidebar')
    @section('sidebar')

    <div class="row">
    <div class="mx-auto rounded mt-8">
        <p style = "font-size:1em" class="badge px-5 py-3 mx-3 fs-2 bg-success text-white text-center rounded"> نام مشتری را در فیلد زیر وارد کنید</p>
    <form action="{{ route('addClient') }}" method="post" name="AddClientForm">
        @csrf
        <div class="col text-right">
            <label class="form-label" > نام مشتری:</label>
            <input type="text" name="ClientName" class="form-control"/>
            {{--<input class="form-control" type="file" name="file" id="upload_file">--}}

        </div>
        <div class="col py-2 text-center">
            <button type="submit" class="btn btn-primary"> ذخیره</button>
        </div>

    </form>
    </div>
</div>
</body>
</html>
