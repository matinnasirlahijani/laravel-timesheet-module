<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">

    <title>مرخصی</title>
    <link rel="icon" href="{{asset('images/logo-80x80.jpg')}}" type="image/x-icon">
    <?php
      use Illuminate\Support\Str;
    ?>

    <!-- Scripts -->
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    {{-- <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script> --}}
    <script src="{{asset('jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('jquery-ui/jquery-ui-1.13.2/jquery-ui.js')}}"></script>
    <link rel="stylesheet" href="{{ asset('DatePicker/dist/css/persian-datepicker.css') }}"/>

  <script src="{{ asset('DatePicker/assets/persian-date.min.js') }}"></script>
  <script src="{{ asset('DatePicker/dist/js/persian-datepicker.js') }}"></script>
  <script>

var monthName = new persianDate().format('MMMM');
console.log(monthName)

    $( function() {
      $( "#datepicker" ).datepicker();
      $('.normal-example').persianDatepicker({
        maxDate: new persianDate().add('day', 30).valueOf(),
        minDate: new persianDate().subtract('day', 40).valueOf(),
        format:'dddd D MMMM YYYY',
        autoClose: true


      });



    } );


    $(function(){


      $('#only-timepicker').datepicker();

      //console.log(new persianDate().toLocale('en').format('YY'));
        $('.only-timepicker-example').persianDatepicker({
          onlyTimePicker: true,
       //format: new persianDate('.only-timepicker-example').toLocale('en').format('H:m') ,
        format: 'H:m',
        calendar:{
        persian: {
            locale: 'en'
        }
    }

});
      });


    $(function() {
    $('#taskdiv').hide();
    $('#service').change(function(){
        if($('#service').val() == 'حسابرسی') {
            $('#taskdiv').show();
        } else {
            $('#taskdiv').hide();
        }
    });
});

$(document).ready(function () {

// On button click, get value
// of input control Show alert
// message box

    var inputString = $("#month").val(monthName);
    //alert(inputString);
    //inputString.value = monthName;

});


    </script>
</head>



<body class="">
    @include('sidebar')
    @section('sidebar')
        <!-- Content -->
        <div class="">

        </div>

    <div class="col py-3 mt-8">

      @if (session('Delete-leave-Done'))
      <div class="col-sm-5">
          <div class="pl-5 pr-5 alert alert-success text-right">
              {{ session('Delete-leave-Done') }}
          </div>
        </div>
      @endif

{{-- <div class=" d-block text-center fs-2"  >
  <p style = "font-size:1em" class="badge px-3 py-3 mx-3 fs-2 bg-primary text-white rounded"> سلام {{Session::get('UserName')}} </p>
</div> --}}
@if (session('leave-success'))
<div class="col-sm-5">
    <div class="pl-5 pr-5 alert alert-success text-right">
        {{ session('leave-success') }}
    </div>
  </div>
@endif
{{-- @if ($getRemainLeaves) --}}
<div class="col-sm-3 mr-2">
<div class="pl-5 pr-5 alert alert-primary text-right">
    {{ $getRemainLeaves }} ساعت مرخصی باقیمانده
</div>
</div>
{{-- @endif --}}
{{-- @if ($getRemainLeaves <= 50)
<div class="col-sm-3">
<div class="pl-5 pr-5 alert alert-danger text-right">
    {{ $getRemainLeaves }} ساعت مرخصی باقیمانده
</div>
</div>
@endif --}}




<div class="form-outline d-block text-right " >
<div class='col-sm-10 col-md-12 col-lg-15 d-block col-sm-9  '>


    <form action="{{ route('addleave') }}" method="post" name="add">
      @csrf
    {{-- <div class="row"> --}}

  <div class="col col-md-3 col-sm-10 col-xsm-12">
    <input name="time_in" style="background-color: #fff" readonly class="only-timepicker-example form-control" id="only-timepicker"/>
    <label class="form-label" for=""> از ساعت</label>
  </div>

  <div class="col col-md-3 col-sm-10 col-xsm-12">
    <input type="text" id="" style="background-color: #fff" readonly name="time_out" class="only-timepicker-example form-control" id="only-timepicker" />
    <label class="form-label" for="">تا ساعت </label>
  </div>
  {{-- <div class="col">
    <input type="text" id="" name="hours" class="form-control" />
    <label class="form-label" for="">به مدت (ساعت)</label>
  </div> --}}
  <div class="col col-md-3 col-sm-10 col-xsm-12" id="">
    <select  id="month" name="month"  class="form-control">
      <option > گزینه ای وارد کنید</option>
      <option selected>فروردین</option>
      <option>اردیبهشت</option>
      <option>خرداد</option>
      <option>تیر</option>
      <option>مرداد</option>
      <option>شهریور</option>
      <option>مهر</option>
      <option>آبان</option>
      <option>آذر</option>
      <option>دی</option>
      <option>بهمن</option>
      <option>اسفند</option>
    </select>
    <label>  در ماه </label>
  </div>
  <div class="col col-md-3 col-sm-10 col-xsm-12">
     <input id="inlineExample form1" readonly style="background-color: #fff" name="date" class="normal-example form-control" />
     <label class="form-label" for="form1">مورخ</label>
  </div>
  <div class="col"></div>

    {{-- <div class="inline-example" ></div> --}}
{{-- </div> --}}
  <br>
</div>
<button type="submit" class="btn btn-primary"> ذخیره </button>
</form>
  </div>

  <div class="alert alert-primary text-right mt-5 col-md-2">
    <h3>مرخصی ها</h3>
  </div>
  <div class="col-sm-12">
  <table class="table table-responsive table-striped">
    <thead>
      <tr>
        <th scope="col"> </th>
        <th scope="col"> شناسه</th>
        <th scope="col"> از ساعت</th>
        <th scope="col"> تا ساعت</th>
        <th scope="col"> مجموع </th>
        <th scope="col"> ماه </th>
        <th scope="col"> تاریخ </th>
        <th scope="col"> حذف </th>



      </tr>
    </thead>
    <tbody>
      @foreach ($leavesQuery as $leaves)
        <tr>
          <td><a href="{{route('editMyLeave' , ['id' => $leaves->id])}}"> ویرایش </a></td>
          <td scope="col">{{ $leaves->id }}</td>
          <td scope="col">{{ str::limit($leaves->time_in,5,'') }}</td>
          <td scope="col">{{ str::limit($leaves->time_out,5,'') }}</td>
          <td scope="col">{{ str::limit($leaves->hours,5,'')}}</td>
          <td scope="col">{{ $leaves->month}}</td>
          <td scope="col">{{ $leaves->date}}</td>
          <td scope="col">
             <a class="btn btn-danger" href="{{route('deleteMyLeave',['id' => $leaves->id])}}">حذف</a></td>



      @endforeach
    </tr>
  {{-- <th class="table-danger">
    مجموع مرخصی {{$month}}
    </th>
      @foreach ($totalLeaves as $totalLeave )
      <tr>

        <td scope="col" class=""> {{$totalLeave->sum}} </td>

      @endforeach --}}

        <tr>
      </tbody>


</table>
</div>
</div>
</div>

</body>
</html>
