<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">

    <title>داشبورد</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    {{-- <script src="https://code.jquery.com/jquery-3.6.0.js"></script> --}}
    <script src="{{asset('jquery/dist/jquery.js')}}"></script>
    {{-- <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script> --}}
    <script src="{{asset('select2/select2.js')}}"></script>
    <!-- Styles -->
    <link href="{{asset('select2/select2.css')}}" rel="stylesheet" />
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="icon" href="{{asset('images/logo-80x80.jpg')}}" type="image/x-icon">
    <?php
      use Illuminate\Support\Str;
    ?>
    <style>
        .c-btn{ border-radius: 10px !important; }
        .c-hover:hover{transition-duration: 500ms ;font-size: 1.2rem !important;}
        select:focus { outline: 2px solid #0071e3 !important; }
    </style>

    <script>

$(function () {
  $(".select2").select2();
});

      $(function () {
        $(".myselect,#myselect").select2();
      });


      $(function () {
        $(".clientSelect").select2();
      });

      $(document).ready(function () {

        // document.getElementById('nameFilter').value = "";

        // console.log(document.getElementById("namefilter").value)


});




    </script>
</head>

<body class="text-right ">
@include('sidebar')
@section('sidebar')
        <div class="col py-3 mt-8">
        <div class="btn-group">

  <div class="">
    {{-- <p class="p-3 pb-2 pl-10 bg-primary text-white rounded"> سلام {{Session::get('UserName')}} <i class="bi bi-person-fill" style="font-size: 1.1rem;"></i></p> --}}

  </div>
  <div class=" row">
 <form class="form-inline text-right px-3" action="{{ route('filter') }}" method="post">
    @csrf
    <div class="form-group col-md text-left">
      <div class="row">
        <label for="inputState" class="px-1">فیلتر بر اساس نام</label>
        <select class="form-control select2" id="inputfilter select2" name="nameFilter" id="namefilter" >
          <option selected>انتخاب کنید</option>
          @foreach($admins as $res)
          <option selected >{{ $res['name'] }}</option>
          @endforeach
        </select>
      </div>

      <div class="row mr-4">
        <label for="month" class="ml-1"> ماه </label>
        <select class="form-control select2" name="month" onchange="this.form.submit();">
          <option selected >  گزینه ای وارد کنید</option>
          <option  >همه ماه ها</option>
          <option  >فروردین</option>
          <option>اردیبهشت</option>
          <option>خرداد</option>
          <option>تیر</option>
          <option>مرداد</option>
          <option>شهریور</option>
          <option>مهر</option>
          <option>آبان</option>
          <option>آذر</option>
          <option>دی</option>
          <option>بهمن</option>
          <option>اسفند</option>
        </select>
      </div>

        </form>
  </div>
        <div class="p-4 d-none d-lg-block"> | </div>
        <hr class="d-lg-none">
        <div class="row">

        <form class="form-inline  text-right px-3" action="{{ route('filterByClient') }}" method="post">
            @csrf
        <label for="inputState" class="px-2">فیلتر بر اساس مشتری </label>
        <select id="inputfilter" name="clientReq" class="form-control clientSelect">
          <option selected>انتخاب کنید</option>
          @foreach($clients as $res)
          <option>{{ $res->clientName }}</option>
          @endforeach
        </select>

        <div class="row mr-1 mt-3">
        <label for="user" class="px-2">کاربر  </label>
        <select id="user" name="user" class="form-control select2" onchange="this.form.submit();">
          <option selected>انتخاب کنید</option>
          <option> همه</option>
          @foreach($admins as $res)
          <option>{{ $res['name'] }}</option>
          @endforeach
        </select>
      </div>
    </div>
</div>

      <div class="dropdown-menu dropdown-menu-left">

        <a class="dropdown-item" name="selectedUser" href="#"></a>

      </div>
</form>

  </div>

  <div class="col col-sm-12 col-md-12 col-lg-12 mx-auto">
    <table class="table table-responsive table-striped ">
     <thead>
    <tr>
      <th scope="col col-md-2">شناسه</th>
      <th scope="col col-md-2">نام</th>
      <th scope="col col-md-2">مشتری</th>
      <th scope="col col-md-2">سرویس</th>
      <th scope="col col-md-2">تخصیص</th>
      <th scope="col col-md-2">ماموریت</th>
      <th scope="col col-md-2">ماه</th>
      <th scope="col col-md-2">نهاری</th>
      <th scope="col col-md-2">ساعت</th>
      <th scope="col col-md-2">تاریخ</th>
    </tr>
  </thead>
  <tbody>

    <tr>


 @foreach ($a as $ab)
     <td scope="col col-md-2">{{ $ab->id}}</td>
     <td scope="col col-md-2">{{ $ab->name}}</td>
     <td scope="col col-md-2">{{ $ab->client }}</td>
     <td scope="col col-md-1">{{ $ab->service }}</td>
     <td scope="col col-md-2">{{ $ab->task }}</td>
     @if ($ab->duty == 1)
    <td scope="col col-md-2">دارد</td>
    @endif
    @if ($ab->duty == 0)
    <td scope="col col-md-2">ندارد</td>
    @endif
     <td scope="col col-md-2">{{ $ab->month }}</td>
     @if ($ab->lunch == 1)
     <td scope="col col-md-2">دارد</td>
     @endif
     @if ($ab->lunch == 0)
     <td scope="col col-md-2">ندارد</td>
     @endif
     <td scope="col col-md-2">{{ str::limit($ab->hours,5,'') }}</td>
     <td scope="col col-md-2">{{ $ab->date }}</td>
     {{-- <td scope="col">{{ verta($ab->created_at) }}</td> --}}
     </tr>
 @endforeach


  </tbody>

</table>
</div>
<nav aria-label="Page navigation example">
  <ul class="pagination">
    <li class="page-item"><a class="page-link" href="{{ $a->previousPageUrl() }}">صفحه قبل</a></li>
    <li class="page-item"><a class="page-link" href="{{ $a->nextPageUrl() }}">صفحه بعد</a></li>
  </ul>
</nav>
<div class="py-3 px-3">



<button class="btn btn-primary c-btn " style="background-color: #0071e3">
  <i class="bi bi-cloud-download text-decoration-none" style="font-size: 1.1rem;">
  <a href="{{route('pdf')}}" class="text-white text-decoration-none c-hover">دانلود گزارش ۳۰ روز گذشته  </i> </a>
</button>



</div></div>
    </div>


</div>

</body>
</html>
