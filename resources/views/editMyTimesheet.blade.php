<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ویرایش</title>
    <link rel="icon" href="{{asset('images/logo-80x80.jpg')}}" type="image/x-icon">


    {{-- <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script> --}}
    <script src="{{asset('jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('jquery-ui/jquery-ui-1.13.2/jquery-ui.js')}}"></script>
    <script src="{{ asset('DatePicker/assets/persian-date.min.js') }}"></script>
    <script src="{{ asset('DatePicker/dist/js/persian-datepicker.js') }}"></script>
    <script src="{{asset('select2/select2.js')}}"></script>
    <!-- Styles -->
    <link href="{{asset('select2/select2.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('DatePicker/dist/css/persian-datepicker.css') }}"/>

    <script>



      $( function() {
        $( "#datepicker" ).datepicker();
        $('.normal-example').persianDatepicker({
            initialValue: false,
          maxDate: new persianDate().add('day', 30).valueOf(),
          minDate: new persianDate().subtract('day', 40).valueOf(),
          format:'dddd D MMMM YYYY',
          autoClose: true


        });



      } );


      $(function(){


        $('#only-timepicker').datepicker();

        //console.log(new persianDate().toLocale('en').format('YY'));
          $('.only-timepicker-example').persianDatepicker({
            onlyTimePicker: true,
         //format: new persianDate('.only-timepicker-example').toLocale('en').format('H:m') ,
          format: 'H:m',
          calendar:{
          persian: {
              locale: 'en'
          }
      }

  });
        });


//       $(function() {

//           if($('#service').val() == 'حسابرسی') {
//               $('#taskdiv').show();
//           } else {
//               $('#taskdiv').hide();
//           }
//       ;
//   });

//   $(function() {
//     //$('#taskdiv').hide();
//     $('#service').change(function(){
//         if($(#service).val() == 'حسابداری'){
//             $('#taskdiv').val() == 'ندارد';
//         }

//         // {
//         //     $('#taskdiv').hide();
//         //     $('#taskdivlable').hide();
//         // }
//     });
// });

        $(function () {
            $("select").select2();
         });

            $(function () {
                $(".myselect,#myselect").select2();
            });

            $(function () {
                $(".clientSelect").select2();
            });
      </script>

</head>
<style>
    hr {
margin-top: 1rem;
margin-bottom: 1rem;
border: 0;
border-top: 1px solid rgba(0, 0, 0, 0.1);
}
  </style>
<body>
    @include('sidebar')
    @section('sidebar')
    <div class="col py-3  mt-8">

        @if (session('ChangesDone'))
<div class="col-sm-5">
    <div class="pl-5 pr-5 alert alert-success text-right">
        <i class="bi bi-check-circle" style="font-size: 1.2rem"></i>
        {{ session('ChangesDone') }}
    </div>
  </div>
@endif

        <div class="d-block text-center fs-2">
            <p style = "font-size:1em" class="badge px-3 py-3 mx-3 fs-2 bg-primary text-white rounded"> ویرایش تایم شیت <i class="bi bi-pencil-square" style="font-size: 1.1rem;"></i> </p>
            <br>
            <a href="{{route('add')}}">
                <button class="btn btn-success p-2 block mb-2">بازگشت به صفحه اصلی</button>
            </a>
        </div>


    <div class="form-outline d-block text-right " >
        <div class='col-sm-10 col-md-12 col-lg-15 d-block col-sm-9 px-sm-10  '>
    <form class="" action="{{ route('updateTimesheet') }}" method="get" >
        <div class="row">
        <div class="col col-sm-8 col-md-4 col-xsm-8 col-lg-3 mx-auto">


        @foreach ($findTimesheetId as $myTimesheet )
        <div class="col">
        <select id="inputfilter" name="client" class="form-control ">
            <option selected >{{ $myTimesheet->client }}</option>
            @foreach ($clients as $myClients)
            <option  >{{ $myClients->clientName }}</option>
            @endforeach



            </div>

                </select>
                <label class="form-label">مشتری</label>
        <div class="col">
            <select type="text" name="fiscalYear" class="form-control" >
                <option value="{{$myTimesheet->fiscalYear}}" selected> {{$myTimesheet->fiscalYear}} </option>
                <option>98</option>
                <option>99</option>
                <option>1400</option>
                <option>1401</option>
                <option>1402</option>
                <option>1403</option>
                <option>1404</option>
                <option>1405</option>

            </select>
            <label class="form-label"> سال مالی</label>
        </div>

        <div class="col">

            <select  name="service" class="form-control" id="service">
                <option selected > حسابداری</option>
                <option selected > حسابرسی</option>
        </select>
            <label class="form-label" for="form1">خدمات</label>
        </div>

        <div class="col">
            <select  name="task" class="form-control" id="taskdiv">

                <option selected value="{{$myTimesheet->task}}"> {{$myTimesheet->task}} </option>
                <option >ندارد </option>
                <option >A - گزارشهای حسابرسی و صورتهای مالی</option>
                <option>B - چک لیست ها</option>
                <option>C - برنامه ریزی کلی و تعیین استراتژی حسابرسی</option>
                <option>D - تأییدیه ها و مکاتبات</option>
                <option>E - برنامه ریزی و ارزیابی آزمون کنترلها</option>
                <option>F - آزمون کنترلها</option>
                <option>G - برنامه ریزی و ارزیابی آزمون محتوا</option>
                <option>H - موجودی نقد و بانک</option>
                <option>I - سرمایه گذاریها</option>
                <option>J - حسابها و اسناد دریافتنی تجاری</option>
                <option>K - سایر حسابها و اسناد دریافتنی</option>
                <option>L - موجودی کالا</option>
                <option>M - پیش پرداختها، سفارشات و سپرده ها</option>
                <option>N - دارایی های ثابت مشهود</option>
                <option>O - دارایی های نامشهود و سایر دارایی ها</option>
                <option>P - حسابها و اسناد پرداختنی</option>
                <option>Q - سایر حسابها و اسناد پرداختنی</option>
                <option>R - پیش دریافتها</option>
                <option>S - مالیات بر عملکرد</option>
                <option>T - تسهیلات مالی دریافتی</option>
                <option>U - حقوق صاحبان سهام</option>
                <option>V - فروش کالا و خدمات</option>
                <option>W - بهای تمام شده</option>
                <option>X - هزینه های تولیدی، خدماتی، اداری، فروش و مالی</option>
                <option>Y - سایر درآمد ها و هزینه های عملیاتی و غیر عملیاتی </option>
                <option>Z - تراز آزمایشی / حسابهای انتظامی / دفاتر قانونی</option>
                <option>A-Z - پرونده دایم</option>
            </select>
            <label class="form-label" for="task" id="taskdivlable">  تخصیص کار</label>
        </div>

        <div class="col">
            <select  name="month" class="form-control" >
                <option value="{{$myTimesheet->month}}" selected> {{$myTimesheet->month}} </option>
                <option >فروردین</option>
                <option>اردیبهشت</option>
                <option>خرداد</option>
                <option>تیر</option>
                <option>مرداد</option>
                <option>شهریور</option>
                <option>مهر</option>
                <option>آبان</option>
                <option>آذر</option>
                <option>دی</option>
                <option>بهمن</option>
                <option>اسفند</option>
            </select>
            <label> ماه </label>
        </div>
        <div class="col-md-1 form-check form-check-inline">
            <select name="duty" id="duty">
                @if ($myTimesheet->duty == 1)
                <option selected value="1" name="duty">دارد</option>
                <option  value="0" name="duty">ندارد</option>
                @endif

                @if ($myTimesheet->duty == 0)
                <option selected value="0" name="duty">ندارد</option>
                <option  value="1" name="duty">دارد</option>

                @endif

            {{-- <option value="1">دارد</option>
            <option value="1">ندارد</option> --}}
            </select>

            <label class="form-check-lable" for="duty">مأموریت</label>

          </div>
        <div class="col">
            <input type="text" readonly style="background-color: #fff" name="time_in" class="only-timepicker-example form-control" id="only-timepicker" value="{{$myTimesheet->time_in}}"/>
            <label class="form-label" for="">ساعت ورود</label>
        </div>

        <div class="col">
            <input type="text" readonly style="background-color: #fff" name="time_out" class="only-timepicker-example form-control" id="only-timepicker" value="{{$myTimesheet->time_out}}"/>
            <label class="form-label" for="">ساعت خروج</label>
        </div>

        <div class="col-md-1 form-check form-check-inline">
            <select name="lunch" id="lunch">
                @if ($myTimesheet->lunch == 1)
                <option selected value="1" name="lunch">دارد</option>
                <option  value="0" name="lunch">ندارد</option>
                @endif

                @if ($myTimesheet->lunch == 0)
                <option selected value="0" name="lunch">ندارد</option>
                <option  value="1" name="lunch">دارد</option>

                @endif

            {{-- <option value="1">دارد</option>
            <option value="1">ندارد</option> --}}
            </select>

            <label class="form-check-lable" for="duty">نهاری</label>

          </div>

        <div class="col">
            <input type="text" id="inlineExample form1" readonly style="background-color: #fff" name="date" class="normal-example form-control" value="{{$myTimesheet->date}}"/>
            <label class="form-label" for="form1">مورخ</label>
        </div>

        <div class="col">
            <input type="text" id="inlineExample form1" name="comment" class="normal-example form-control" value="{{$myTimesheet->comments}}"/>
            <label class="form-label" for="form1">توضیحات</label>
        </div>

    </div>



        @endforeach
        <input type="hidden" name="id" value="{{$id}}"/>
        <div>
            <br>

        <button class="btn btn-success" type="submit"> ویرایش

        </button> </div>
        </div>
        </div>
    </form>
    </div>

</body>
</html>
