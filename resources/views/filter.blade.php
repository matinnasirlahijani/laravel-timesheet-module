<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}"><head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php
        use Carbon\Carbon;
        $carbon = new Carbon;


    ?>
    <title>فیلتر بر اساس نام کاربر</title>
    <link rel="icon" href="{{asset('images/logo-80x80.jpg')}}" type="image/x-icon">
    <?php
    use Illuminate\Support\Str;
  ?>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    {{--<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>--}}
    <script type="text/javascript" src="{{asset('js/loader.js')}}"></script>
    <script type="text/javascript">
    //   google.charts.load('current', {'packages':['corechart']});
    //   google.charts.setOnLoadCallback(drawChart);

    //   function drawChart() {

    //     var data = google.visualization.arrayToDataTable([
    //       ['Task', 'Hours per Day'],
    //       @php
    //             foreach($query as $cli) {
    //                 echo ("['".$cli->client."', ".$cli->sum.", ],");
    //             }
    //             @endphp
    //     ]);

    //     var options = {
    //       //pieHole: 0.5,
    //       title: 'مجموع ساعات در هر شرکت',
    //       pieSliceTextStyle: {
    //         color: 'black',
    //       },
    //       pieSliceText:'200px',
    //       legend: 'none',

    //     };


    //     var chart = new google.visualization.PieChart(document.getElementById('piechart'));

    //     chart.draw(data, options);
    //   }
    </script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body>
  @include('sidebar')
  @section('sidebar')
  <div class="mx-auto">

      </div>
  <div class="col py-3 mt-8">
  <div class="d-block text-center fs-2">
      <p style = "font-size:1em" class="badge px-3 py-3 mx-3 fs-2 bg-primary text-white rounded"> {{$filter}} <i class="bi bi-person-fill" style="font-size: 1.1rem;"></i> </p>
  </div>
<div class="container text-right">
    <div class="row">
    {{-- <table class="table table-striped">
        <thead>
          <tr>
            <th scope="col">نام</th>
            <th scope="col">مشتری</th>
            <th scope="col">سرویس</th>
            <th scope="col">ساعت</th>
            <th scope="col">تاریخ</th>

          </tr>
        </thead>
        <tbody>

          <tr>

         @foreach($getTime as $sort)
        <td scope="col">{{ $sort->name}}</td>
     <td scope="col">{{ $sort->client }}</td>
     <td scope="col">{{ $sort->service }}</td>
     <td scope="col">{{ $sort->hours }}</td>
     <td scope="col">{{ $sort->date }}</td>
    </tr>

        @endforeach
        <tr>
          <th class="table-success" scope="col">مجموع ساعات کاربر</th>
        </tr>
        <tr>
          @foreach ( $getclientsum as $sum )


          <td scope="col">{{ $sum->sum }}</td>
          @endforeach
        </tr>


    </div>
</tbody>

</table> --}}

{{-- <div  id="piechart" style="width: 80em; height: 50em;"></div> --}}
<div class="col-sm-12">
<table class="table table-responsive table-striped">
  <thead>
    <tr>
      <th scope="col"> نام واحد مورد رسیدگی</th>
      <th scope="col">سال مالی</th>
      <th scope="col"> ماه </th>
      <th scope="col"> تخصیص </th>
      <th scope="col"> ماموریت </th>
      <th scope="col">از ساعت  </th>
      <th scope="col">تا ساعت</th>
      <th scope="col">تاریخ</th>
      <th scope="col">نهاری</th>
      <th scope="col">جمع ساعت</th>
      <th scope="col"> توضیحات</th>


    </tr>
  </thead>
  <tbody>



   @foreach($getTime as $data)
   <tr>
  <td scope="col">{{ $data->client}}</td>
<td scope="col">{{ $data->fiscalYear }}</td>
<td scope="col">{{ $data->month }}</td>
<td scope="col">{{ $data->task }}</td>
@if ($data->duty == 1)
<td scope="col">دارد</td>
@endif
@if ($data->duty == 0)
<td scope="col">ندارد</td>
@endif
<td scope="col">{{ $data->time_in }}</td>
<td scope="col">{{ $data->time_out }}</td>
<td scope="col">{{ $data->date }}</td>
@if ($data->lunch == 1)
<td scope="col">دارد</td>
@endif
@if ($data->lunch == 0)
<td scope="col">ندارد</td>
@endif
<td scope="col">{{ str::limit($data->hours, 5 ,'') }}</td>
<td scope="col">{{ $data->comments }}</td>

@endforeach

</tr>

  <tr>
    <th class="table-success" scope="col">  مجموع ساعات کاربر {{$filter }} در {{$month}}</th>
    <th class="table-success" scope="col">   مجموع ساعات کاربر با احتساب مرخصی </th>

  </tr>
  <tr>
    @foreach ( $getclientsum as $sum )
    <td scope="col">{{ $sum->sum }}</td>
    @endforeach
   {{-- <td scope="col">  {{$calculateTotalIncludLeave}}  </td> --}}
   <td scope="col">  {{$total}}  </td>
  </tr>


  <tr>
    @if ($overTime)
    <tr scope="col"> مجموع اضافه کاری : {{$overTime}} ساعت</tr>
    @endif
  </tr>


  <th class="table-warning">
    ماموریت این ماه
</th>
<tr>
    <td>
        {{$dutyCountByMonth}}
    </td>
</tr>

<th class="table-warning">
      مجموع ماموریت ها
</th>
<tr>
    <td>
        {{$totalDuty}}
    </td>
</tr>


<th class="table-warning">
      مانده مرخصی
</th>


<tr>
    <td>
        {{-- {{$remainLeaves}} --}}


           {{$ConvertTotalLeavesInYearToHours }} ساعت

    </td>
</tr>

<th class="table-warning">
    موظفی این ماه
</th>


<tr>
  <td>
      {{-- {{$remainLeaves}} --}}


         {{$dynamic_overtime }} ساعت

  </td>
</tr>




</tr>
</br>
<tr>
    @if ($totalLeavesInYear)
    <tr scope="col"> مجموع مرخصی از اول سال تا اکنون: {{$totalLeavesInYear->sum}} ساعت</tr>
    @endif
  </tr>
<tr>
</div>
</tbody>

</table>
</div>
  @if ($month !=='همه ماه ها')
  <div class="alert alert-primary text-right">
    <h3>مرخصی ها</h3>
  </div>
  <table class="table table-responsive table-striped">
    <thead>
      <tr>
        <th scope="col"> از ساعت</th>
        <th scope="col"> تا ساعت</th>
        <th scope="col"> مجموع </th>
        <th scope="col"> ماه </th>
        <th scope="col"> تاریخ </th>


      </tr>

    </thead>
    <tbody>
      @foreach ($leavesQuery as $leaves)
        <tr>
          <td scope="col">{{ str::limit($leaves->time_in,5,'') }}</td>
          <td scope="col">{{ str::limit($leaves->time_out,5,'') }}</td>
          <td scope="col">{{ str::limit($leaves->hours,5,'')}}</td>
          <td scope="col">{{ $leaves->month}}</td>
          <td scope="col">{{ $leaves->date}}</td>

      @endforeach
    </tr>

  <th class="table-danger">
    مجموع مرخصی {{$month}}
    </th>
      {{-- @foreach ($totalLeaves as $totalLeave ) --}}
      <tr>
        @if ($totalLeaves)

        <td scope="col" class=""> {{$totalLeaves->sum}} </td>
        @endif

      {{-- @endforeach --}}


      </tbody>
  @endif

</table>
{{-- <ul class="pagination">
  <li><a  href= "{{ $getTime->appends(['nameFilter' => $filter])->links() }} "></a>

</li></ul>
</br> --}}

 {{-- @foreach ($overTime as $userOverTime )

 @endforeach --}}




 <form action="{{ route('generatefilterpdf') }}" method="get">
  <input type="hidden" name="nameFilter" value="{{$filter}}"/>
  <input type="hidden" name="month" value="{{$month}}"/>
  <button class="btn btn-success">
    <i class="bi bi-cloud-download text-decoration-none" style="font-size: 1.1rem;"></i>
    دانلود </button>
  </form>

</body>
</html>
