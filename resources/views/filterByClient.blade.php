<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}"><head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>فیلتر بر اساس مشتریان</title>
    <link rel="icon" href="{{asset('images/logo-80x80.jpg')}}" type="image/x-icon">
    <?php
    use Illuminate\Support\Str;
  ?>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    @include('sidebar')
    @section('sidebar')
    <div class="mx-auto">

    </div>
    <div class="col py-3 mt-8">
    <div class="d-block text-center fs-2">
      <p style = "font-size:1em" class="badge px-3 py-3 mx-3 fs-2 bg-primary text-white rounded"> {{"مشتری " .$reqFilter}} <i class="bi bi-person-fill" style="font-size: 1.1rem;"></i> </p>
  </div>
<div class="container text-right">
    <div class="row">
    <table class="table table-responsive table-striped">
        <thead>
          <tr>
            <th scope="col">نام</th>
            <th scope="col">سال مالی</th>
            <th scope="col"> ماه </th>
            <th scope="col">مشتری</th>
            <th scope="col">سرویس</th>
            <th scope="col"> تخصیص </th>
            <th scope="col">ساعت</th>
            <th scope="col"> ماه</th>
            <th scope="col">تاریخ</th>
            <th scope="col">توضیحات</th>


          </tr>
        </thead>
        <tbody>

          <tr>

         @foreach($getclient as $sort)

        <td scope="col">{{ $sort->name}}</td>
        <td scope="col">{{ $sort->fiscalYear }}</td>
        <td scope="col">{{ $sort->month }}</td>
        <td scope="col">{{ $sort->client }}</td>
        <td scope="col">{{ $sort->service }}</td>
        <td scope="col">{{ $sort->task }}</td>
        <td scope="col">{{ str::limit($sort->hours,5,'') }}</td>
        <td scope="col">{{ $sort->month }}</td>
        <td scope="col">{{ $sort->date }}</td>
        <td scope="col">{{ $sort->comments }}</td>


     </tr>
        @endforeach
        <tr>
            <th class="table-success" scope="col">مجموع ساعات</th>


        </tr>

          @foreach ( $getclientsum as $sum )
          <tr>
            <td scope="col"> {{$sum->name}}</td>
            <td scope="col">{{ $sum->sum }}</td>
            @endforeach
        </tr>

    </div>

</tbody>
</table>

{{-- <ul class="pagination">
    <li class="page-item"><a class="page-item" href="{{ $getclient->appends(['clientReq' => $reqFilter])->links() }}">

  </a></li></ul> --}}

</div>

@if ($user !== "همه")
<div>

  <h3> کارت پروژه</h3>
</div>
<table class="table table-responsive table-striped">
  <thead>


    <tr>
      <td> نام کاربر</td>

      @foreach($getTotalHoursByMonth as $sort)

      <td scope="col">{{ $sort->month }}</td>

      @endforeach
      <td> مشتری</td>
    </tr>
  </thead>
  <tbody>






    <tr>
      <td scope="col"> {{$user}} </td>

   @foreach($getTotalHoursByMonth as $sort)

  <td scope="col">{{ $sort->sum}}</td>

  @endforeach
  <td scope="col"> {{$reqFilter}} </td>
</tr>
  {{-- <tr>
      <th class="table-success" scope="col">مجموع ساعات</th>


  </tr>

    @foreach ( $getclientsum as $sum )
    <tr>
      <td scope="col"> {{$sum->name}}</td>
      <td scope="col">{{ $sum->sum }}</td>
      @endforeach
  </tr> --}}

</div>

</tbody>
</table>

@endif
<form action="{{ route('generateClientpdf') }}" method="get">
<input type="hidden" name="clientReq" value="{{$reqFilter}}"/>
<input type="hidden" name="user" value="{{$user}}"/>
<button class="btn btn-success">
  <i class="bi bi-cloud-download text-decoration-none" style="font-size: 1.1rem;"></i>
  دانلود </button>
</form>
</body>
</html>
