<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->id();
            $table->text('name');
            $table->text('email');
            $table->text('password');
            $table->bool('is_admin');
            $table->timestamps();
        });
        Schema::create('admins_time', function (Blueprint $table) {
            $table->id();
            $table->text('name');
            $table->unsignedInteger('admin_time_id');
            $table->text('client');
            $table->text('service');
            $table->integer('hours');
            $table->text('date');
            $table->unsignedInteger('client_time_id');
            $table->timestamps();
            $table->foreign('admin_time_id')->refrences('id')->on('admins')->onDelete('cascade');
        });

        Schema::create('client', function (Blueprint $table) {
            $table->id();
            $table->text('clientName');
            $table->timestamps();
        });
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
        
    }
}
