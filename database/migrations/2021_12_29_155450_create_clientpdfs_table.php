<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientpdfsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientpdfs', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('clientID');
            $table->string('pdf');
            $table->timestamps();
            $table->unsignedInteger('clientsheet_id');
            $table->foreign('ClientID')->refrences('id')->on('clientsheets')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientpdfs');
    }
}
